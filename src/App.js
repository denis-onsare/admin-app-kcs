import React, { Component } from "react";
import { Switch, Route } from "react-router-dom";
import "./App.scss"
import GuestRoute from "./components/routes/GuestRoute";
import AdminRoute from "./components/routes/AdminRoute";
// Containers
const DefaultLayout = React.lazy(() => import('./containers/DefaultLayout'));

const loading = () => <div className="animated fadeIn pt-3 text-center">Loading...</div>;

// Pages
const LoginPage = React.lazy(() => import('./views/Pages/Login/LoginPage'));
const Register = React.lazy(() => import('./views/Pages/Register'));
const Page404 = React.lazy(() => import('./views/Pages/Page404'));
const Page500 = React.lazy(() => import('./views/Pages/Page500'));


class App extends Component {
  render() {
    return (
      <div>
      <React.Suspense fallback={loading()}>
        <Switch>
          <GuestRoute exact path="/login" name="Login Page" component={LoginPage} />
         
          <GuestRoute
            exact
            path="/register"
            name="Register Page"
            component={Register}
          />
          <Route exact path="/404" name="Page 404" render={props => <Page404 {...props}/>}/>
          <Route exact path="/500" name="Page 500" render={props => <Page500 {...props}/>} />
          <AdminRoute path="/" name="Home" component={DefaultLayout} />
        </Switch>
      </React.Suspense>
      </div>
    );
  }
}

export default App;
