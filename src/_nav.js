export default {
  items: [
    {
      name: "Dashboard",
      url: "/dashboard",
      icon: "icon-speedometer"
    },
    // {
    //   name: "Posts",
    //   url: "/posts",
    //   icon: "icon-note",
    //   children: [
    //     {
    //       name: "All",
    //       url: "/posts",
    //       icon: "icon-star"
    //     },
    //     {
    //       name: "Add New",
    //       url: "/posts/new",
    //       icon: "icon-star"
    //     }
    //   ]
    // },
    // {
    //   name: "Leadership",
    //   url: "/leadership",
    //   icon: "icon-people",
    //   children: [
    //     {
    //       name: "Structure",
    //       url: "/leadership/structure",
    //       icon: "icon-star"
    //     },
    //     {
    //       name: "Add Leadership",
    //       url: "/leadership/add",
    //       icon: "icon-star"
    //     }
    //   ]
    // },

    {
      name: "Members",
      url: "/membership",
      icon: "icon-people",
      children: [
        {
          name: "All",
          url: "/members",
          icon: "icon-star"
        },
        {
          name: "Add New",
          url: "/members/new",
          icon: "icon-star"
        }
      ]
    },
    // {
    //   name: "Events",
    //   url: "/events",
    //   icon: "icon-map",
    //   children: [
    //     {
    //       name: "All",
    //       url: "/events",
    //       icon: "icon-star"
    //     },
    //     {
    //       name: "Add New",
    //       url: "/events/new",
    //       icon: "icon-star"
    //     }
    //   ]
    // },
    {
      name: "Meetings",
      url: "/meetings",
      icon: "icon-location-pin",
      children: [
        {
          name: "All",
          url: "/meetings",
          icon: "icon-star"
        },
        {
          name: "Add New",
          url: "/meetings/new",
          icon: "icon-star"
        }
      ]
    },
    {
      name: "Past Activities",
      url: "/past-activities",
      icon: "icon-clock",
      children: [
        {
          name: "All",
          url: "/past-activities",
          icon: "icon-star"
        },
        {
          name: "Add New",
          url: "/past-activities/new",
          icon: "icon-star"
        }
      ]
    },
    {
      name: "Journals",
      url: "/journals",
      icon: "icon-layers",
      children: [
        {
          name: "All",
          url: "/journals",
          icon: "icon-star"
        },
        {
          name: "Add New",
          url: "/journals/new",
          icon: "icon-star"
        }
      ]
    },
    {
      name: "Topical issues",
      url: "/topical-issues",
      icon: "icon-list",
      children: [
        {
          name: "All",
          url: "/topical-issues",
          icon: "icon-star"
        },
        {
          name: "Add New",
          url: "/topical-issues/new",
          icon: "icon-star"
        }
      ]
    },
      {
      name: "Courses",
      url: "/courses",
      icon: "icon-layers",
      children: [
        {
          name: "All",
          url: "/courses",
          icon: "icon-star"
        },
        {
          name: "Add New",
          url: "/courses/new",
          icon: "icon-star"
        }
      ]
    },
  ]
};
