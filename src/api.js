import axios from "axios";

const apiUrl = "https://kenyachemicalsociety.org/app1/api";
// const apiUrl = "http://localhost:8082/api";

export default {
  user: {
    fetchUsers: () => axios.get(`${apiUrl}/users`).then(res => res.data),
    register: user => axios.post(`${apiUrl}/users`, user).then(res => res.data),
    login: credentials =>
      axios.post(`${apiUrl}/admin/signin`, credentials).then(res => res.data),
    confirm: token =>
      axios.post(`${apiUrl}/users/confirmation`, token).then(res => res.data),
    resetPasswordRequest: email =>
      axios.post(`${apiUrl}/users/reset_password_request`, email),
    validateToken: token => axios.post(`${apiUrl}/users/validate_token`, token),
    resetPassword: data => axios.post(`${apiUrl}/users/reset_password`, data),
    fetchCurrentUser: () =>
      axios.get(`${apiUrl}/users/current_user`).then(res => res.data),
    updateUser: data =>
      axios
        .put(`${apiUrl}/admin/users/${data._id}`, data)
        .then(res => res.data),
    deleteUser: id =>
      axios.delete(`${apiUrl}/admin/users/${id}`).then(res => res.data)
  },
  meeting: {
    postMeeting: meeting =>
      axios
        .post(`${apiUrl}/meetings`, meeting, {
          headers: { "Content-type": "multipart/form-data" }
        })
        .then(res => res.data),
    fetchMeetings: () => axios.get(`${apiUrl}/meetings`).then(res => res.data),
    fetchMeeting: id =>
      axios.get(`${apiUrl}/meetings/${id}`).then(res => res.data),
    updateMeeting: data =>
      axios
        .put(`${apiUrl}/meetings/${data.get("_id")}`, data, {
          headers: { "Content-type": "multipart/form-data" }
        })
        .then(res => res.data),
    deleteMeeting: id =>
      axios.delete(`${apiUrl}/meetings/${id}`).then(res => res.data)
  },
  activity: {
    postActivity: activity =>
      axios
        .post(`${apiUrl}/activities`, activity, {
          headers: { "Content-type": "multipart/form-data" }
        })
        .then(res => res.data),
    fetchActivities: () =>
      axios.get(`${apiUrl}/activities`).then(res => res.data),
    fetchActivity: id =>
      axios.get(`${apiUrl}/activities/${id}`).then(res => res.data),
    updateActivity: data =>
      axios
        .put(`${apiUrl}/activities/${data.get("_id")}`, data, {
          headers: { "Content-type": "multipart/form-data" }
        })
        .then(res => res.data),
    deleteActivity: id =>
      axios.delete(`${apiUrl}/activities/${id}`).then(res => res.data)
  },
  journal: {
    postJournal: journal =>
      axios.post(`${apiUrl}/journals`, journal).then(res => res.data),
    fetchJournals: () => axios.get(`${apiUrl}/journals`).then(res => res.data),
    fetchJournal: id =>
      axios.get(`${apiUrl}/journals/${id}`).then(res => res.data),
    updateJournal: data =>
      axios
        .put(`${apiUrl}/journals/${data.get("_id")}`, data)
        .then(res => res.data),
    deleteJournal: id =>
      axios.delete(`${apiUrl}/journals/${id}`).then(res => res.data)
  },
  course: {
    postCourse: course =>
      axios
        .post(`${apiUrl}/courses`, course, {
          headers: { "Content-type": "multipart/form-data" }
        })
        .then(res => res.data),
    fetchCourses: () => axios.get(`${apiUrl}/courses`).then(res => res.data),
    fetchCourse: id =>
      axios.get(`${apiUrl}/courses/${id}`).then(res => res.data),
    updateCourse: data =>
      axios
        .put(`${apiUrl}/courses/${data.get("_id")}`, data, {
          headers: { "Content-type": "multipart/form-data" }
        })
        .then(res => res.data),
    deleteCourse: id =>
      axios.delete(`${apiUrl}/courses/${id}`).then(res => res.data)
  },
  topicalIssue: {
    postIssue: issue =>
      axios.post(`${apiUrl}/issues`, issue).then(res => res.data),
    fetchIssues: () => axios.get(`${apiUrl}/issues`).then(res => res.data),
    fetchIssueById: id =>
      axios.get(`${apiUrl}/issues/${id}`).then(res => res.data),
    updateIssue: data =>
      axios
        .put(`${apiUrl}/issues/${data.get("_id")}`, data, {
          headers: { "Content-type": "multipart/form-data" }
        })
        .then(res => res.data),
    deleteIssue: id =>
      axios.delete(`${apiUrl}/issues/${id}`).then(res => res.data)
  },
  posts: {
    postPost: post => axios.post(`${apiUrl}/posts`, post).then(res => res.data),
    fetchPosts: () => axios.get(`${apiUrl}/posts`).then(res => res.data),
    fetchPost: id => axios.get(`${apiUrl}/posts/${id}`).then(res => res.data),
    updatePost: data =>
      axios.put(`${apiUrl}/posts/${data._id}`, data).then(res => res.data),
    deletePost: id =>
      axios.delete(`${apiUrl}/posts/${id}`).then(res => res.data)
  },
  students: {
    fetchStudents: () => axios.get(`${apiUrl}/students`).then(res => res.data)
  },
  leadership: {
    postLeadership: leader =>
      axios.post(`${apiUrl}/leadership`, leader).then(res => res.data),
    fetchLeadership: () =>
      axios.get(`${apiUrl}/leadership`).then(res => res.data)
  },
  messages: {
    fetchMessages: () => axios.get(`${apiUrl}/messages`).then(res => res.data)
  },
  payments: {
    fetchPayments: () =>
      axios.get(`${apiUrl}/ipn/payments`).then(res => res.data)
  }
};
