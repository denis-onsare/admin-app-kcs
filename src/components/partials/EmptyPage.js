import React from "react";
import { Container, Row, Col } from "reactstrap";
import Ionicon from "react-ionicons";

const EmptyPage = ({ message }) => (
  <Container>
    <Row>
      <Col
        style={{ textAlign: "center", margin: "50px auto" }}
        sm={{ size: "6", offset: "3" }}
      >
        <Ionicon icon="ios-information-circle" fontSize="60px" color="grey" />
        <h5>No {message} available at the moment</h5>
      </Col>
    </Row>
  </Container>
);

export default EmptyPage;
