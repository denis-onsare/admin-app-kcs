import React, { Component } from "react";
import {
  Badge,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown
} from "reactstrap";
import PropTypes from "prop-types";

import {
  AppHeaderDropdown,
  AppNavbarBrand,
  AppSidebarToggler
} from "@coreui/react";
import logo from "../../assets/img/brand/logo.JPG";
import sygnet from "../../assets/img/brand/sygnet.svg";

import { connect } from "react-redux";
import { logout } from "../../store/actions/user";
import { fetchMessages } from "../../store/actions/messages";

const propTypes = {
  children: PropTypes.node
};

const defaultProps = {};

class DefaultHeader extends Component {
  componentDidMount = () => {
    this.props.fetchMessages();
  };
  render() {
    // eslint-disable-next-line
    const { logout, children, ...attributes } = this.props;

    return (
      <React.Fragment>
        <AppSidebarToggler className="d-lg-none" display="md" mobile />
        <AppNavbarBrand
          full={{ src: logo, width: 89, height: 25, alt: "CoreUI Logo" }}
          minimized={{ src: sygnet, width: 30, height: 30, alt: "CoreUI Logo" }}
        />
        <AppSidebarToggler className="d-md-down-none" display="lg" />

        <Nav className="d-md-down-none" navbar>
          {/* <NavItem className="px-3">
            <NavLink href="/">Home</NavLink>
          </NavItem> */}
          <NavItem className="px-3">
            <NavLink href="/users">Users</NavLink>
          </NavItem>
        </Nav>
        <Nav className="ml-auto" navbar>
          <NavItem className="d-md-down-none">
            <NavLink href="/messages">
              <i className="icon-bell" />
              <Badge pill color="danger">
                {this.props.messages.length}
              </Badge>
            </NavLink>
          </NavItem>

          <AppHeaderDropdown direction="down">
            <UncontrolledDropdown>
              <DropdownToggle nav>
                <img
                  style={{ border: "2px solid green" }}
                  src={this.props.user.avatar}
                  className="img-avatar"
                  alt={this.props.user.firstName}
                />
              </DropdownToggle>
              <DropdownMenu right>
                <DropdownItem header tag="div" className="text-center">
                  <strong>Account</strong>
                </DropdownItem>

                <DropdownItem href="/messages">
                  <i className="fa fa-envelope" /> Messages
                  <Badge color="success">{this.props.messages.length}</Badge>
                </DropdownItem>
                <DropdownItem href="/comments">
                  <i className="fa fa-comments" /> Comments
                  <Badge color="warning">0</Badge>
                </DropdownItem>
                <DropdownItem href="/settings">
                  <i className="fa fa-wrench" /> Settings
                </DropdownItem>

                <DropdownItem href="/payments">
                  <i className="fa fa-usd" /> Payments
                  <Badge color="secondary">{this.props.payments.length}</Badge>
                </DropdownItem>

                <DropdownItem divider />

                <DropdownItem onClick={() => logout()}>
                  <i className="fa fa-lock" /> Logout
                </DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
          </AppHeaderDropdown>
        </Nav>
        {/* <AppAsideToggler className="d-md-down-none" /> */}
        {/*<AppAsideToggler className="d-lg-none" mobile />*/}
      </React.Fragment>
    );
  }
}

DefaultHeader.propTypes = propTypes;
DefaultHeader.defaultProps = defaultProps;

function mapStateToProps(state) {
  return {
    user: state.user,
    messages: state.messages,
    payments: state.payments
  };
}

export default connect(mapStateToProps, { logout, fetchMessages })(
  DefaultHeader
);
