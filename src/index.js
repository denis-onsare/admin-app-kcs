import "./polyfill";
import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import {BrowserRouter } from "react-router-dom";

import decode from "jwt-decode";
import { userLoggedIn, userLoggedOut } from "./store/actions/user";

import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import { Provider } from "react-redux";
import { composeWithDevTools } from "redux-devtools-extension";
import rootReducer from "./store/reducers/rootReducer";
import setAuthorizationHeader from "./utils/setAuthorizationHeader";

// disable ServiceWorker
// import registerServiceWorker from './registerServiceWorker';

const store = createStore(
  rootReducer,
  composeWithDevTools(applyMiddleware(thunk))
);

if (localStorage.kcsAdminWebToken) {
  const payload = decode(localStorage.kcsAdminWebToken);
  //logout user on token expiry
  const currentTime = Date.now() / 1000;
  if (currentTime > payload.exp) {
    store.dispatch(userLoggedOut());
    // window.location = "/login";
  } else {
    const user = {
      title: payload.title,
      firstName: payload.firstName,
      email: payload.email,
      avatar: payload.avatar,
      chapter: payload.chapter,
      token: localStorage.mcdpWebToken,
      confirmed: payload.confirmed,
      createdAt: payload.createdAt,
      updatedAt: payload.updatedAt
    };
    setAuthorizationHeader(localStorage.kcsAdminWebToken);
    store.dispatch(userLoggedIn(user));
  }
}

ReactDOM.render(
  <BrowserRouter>
    <Provider store={store}>
      <App />
    </Provider>
  </BrowserRouter>,
  document.getElementById("root")
);
// disable ServiceWorker
// registerServiceWorker();
