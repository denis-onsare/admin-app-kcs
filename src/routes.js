import React from "react";

// import DefaultLayout from "./containers/DefaultLayout";

const Dashboard = React.lazy(() => import("./views/Dashboard"));
const Users = React.lazy(() => import("./views/Users/Users"));
const User = React.lazy(() => import("./views/Users/User"));
const Posts = React.lazy(() => import("./views/Posts/Posts"));
const Post = React.lazy(() => import("./views/Posts/Post"));
const CreatePost = React.lazy(() => import("./views/Posts/CreatePost"));
const Members = React.lazy(() => import("./views/Members/Members"));
const Member = React.lazy(() => import("./views/Members/Member"));
const CreateMember = React.lazy(() => import("./views/Members/CreateMember"));
const Events = React.lazy(() => import("./views/Events/Events"));
const Event = React.lazy(() => import("./views/Events/Event"));
const CreateEvent = React.lazy(() => import("./views/Events/CreateEvent"));
const Meetings = React.lazy(() => import("./views/Meetings/Meetings"));
const Meeting = React.lazy(() => import("./views/Meetings/Meeting"));
const CreateMeeting = React.lazy(() =>
  import("./views/Meetings/CreateMeeting")
);
const PastActivities = React.lazy(() =>
  import("./views/PastActivities/PastActivities")
);
const PastActivity = React.lazy(() =>
  import("./views/PastActivities/PastActivity")
);
const CreatePastActivity = React.lazy(() =>
  import("./views/PastActivities/CreatePastActivity")
);
const Journals = React.lazy(() => import("./views/Journals/Journals"));
const Journal = React.lazy(() => import("./views/Journals/Journal"));
const CreateJournal = React.lazy(() =>
  import("./views/Journals/CreateJournal")
);
const TopicalIssues = React.lazy(() =>
  import("./views/TopicalIssues/TopicalIssues")
);
const TopicalIssue = React.lazy(() =>
  import("./views/TopicalIssues/TopicalIssue")
);
const CreateTopicalIssue = React.lazy(() =>
  import("./views/TopicalIssues/CreateTopicalIssue")
);
const Structure = React.lazy(() => import("./views/Leadership/Structure"));
const AddLeader = React.lazy(() => import("./views/Leadership/AddLeader"));
const Messages = React.lazy(() => import("./views/Messages/Messages.js"));
const Payments = React.lazy(() => import("./views/Payments/Payments.js"));
const Comments = React.lazy(() => import("./views/Comments/Comments.js"));
const Setting = React.lazy(() => import("./views/Setting/Settings.js"));
const Notifications = React.lazy(() =>
  import("./views/Notifications/Notifications.js")
);
const Courses = React.lazy(() => import("./views/Courses/Courses"));
const Course = React.lazy(() => import("./views/Courses/Course"));
const CreateCourse = React.lazy(() => import("./views/Courses/createCourse"));
const CourseStats = React.lazy(() => import("./views/Courses/CourseStats"));

// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const routes = [
  { path: "/", exact: true, name: "Home" },
  { path: "/dashboard", name: "Dashboard", component: Dashboard },
  { path: "/posts", exact: true, name: "Posts", component: Posts },
  { path: "/messages", exact: true, name: "Messages", component: Messages },
  { path: "/payments", exact: true, name: "Payments", component: Payments },
  { path: "/comments", exact: true, name: "Comments", component: Comments },
  { path: "/settings", exact: true, name: "Settings", component: Setting },
  {
    path: "/notifications",
    exact: true,
    name: "Notifications",
    component: Notifications
  },

  {
    path: "/leadership/structure",
    exact: true,
    name: "Structure",
    component: Structure
  },

  {
    path: "/leadership/add",
    exact: true,
    name: "Add Leadership",
    component: AddLeader
  },
  {
    path: "/posts/new",
    exact: true,
    name: "Create Post",
    component: CreatePost
  },
  { path: "/posts/:_id", exact: true, name: "Post Details", component: Post },
  {
    path: "/posts/edit/:_id",
    exact: true,
    name: "Edit Post",
    component: CreatePost
  },
  { path: "/members", exact: true, name: "Members", component: Members },
  {
    path: "/members/new",
    exact: true,
    name: "Create Member",
    component: CreateMember
  },
  {
    path: "/members/:id",
    exact: true,
    name: "Members Details",
    component: Member
  },
  { path: "/events", exact: true, name: "Events", component: Events },
  {
    path: "/events/new",
    exact: true,
    name: "Create Event",
    component: CreateEvent
  },
  { path: "/events/:id", exact: true, name: "Event Details", component: Event },
  { path: "/meetings", exact: true, name: "Meetings", component: Meetings },
  {
    path: "/meetings/new",
    exact: true,
    name: "Create Meeting",
    component: CreateMeeting
  },
  {
    path: "/meetings/edit/:_id",
    exact: true,
    name: "Edit Meeting",
    component: CreateMeeting
  },
  {
    path: "/meetings/:_id",
    exact: true,
    name: "Meeting Details",
    component: Meeting
  },
  {
    path: "/past-activities",
    exact: true,
    name: "Past Activities",
    component: PastActivities
  },
  {
    path: "/past-activities/new",
    exact: true,
    name: "Create Past Activity",
    component: CreatePastActivity
  },
  {
    path: "/past-activities/:_id",
    exact: true,
    name: "Activity Details",
    component: PastActivity
  },
  {
    path: "/past-activities/edit/:_id",
    exact: true,
    name: "Edit Activity",
    component: CreatePastActivity
  },
  {
    path: "/journals",
    exact: true,
    name: "Journals",
    component: Journals
  },
  {
    path: "/journals/new",
    exact: true,
    name: "Create Journal",
    component: CreateJournal
  },
  {
    path: "/journals/:_id",
    exact: true,
    name: "Journals Details",
    component: Journal
  },
  {
    path: "/journals/edit/:_id",
    exact: true,
    name: "Edit Journal",
    component: CreateJournal
  },
  {
    path: "/topical-issues",
    exact: true,
    name: "Topical issues",
    component: TopicalIssues
  },
  {
    path: "/topical-issues/new",
    exact: true,
    name: "Create Topical Issue",
    component: CreateTopicalIssue
  },
  {
    path: "/topical-issues/:_id",
    exact: true,
    name: "Topical Issue Details",
    component: TopicalIssue
  },
  {
    path: "/topical-issues/edit/:_id",
    exact: true,
    name: "Edit Topical Issue",
    component: CreateTopicalIssue
  },

  { path: "/users", exact: true, name: "Users", component: Users },
  { path: "/users/:id", exact: true, name: "User Details", component: User },
  {
    path: "/courses",
    exact: true,
    name: "Courses",
    component: Courses
  },
  {
    path: "/courses/new",
    exact: true,
    name: "Create Course",
    component: CreateCourse
  },
  {
    path: "/courses/:_id",
    exact: true,
    name: "Courses Details",
    component: Course
  },
  {
    path: "/courses/statistics/:_id",
    exact: true,
    name: "Courses Stats",
    component: CourseStats
  },
  {
    path: "/courses/edit/:_id",
    exact: true,
    name: "Edit Course",
    component: CreateCourse
  }
];

export default routes;
