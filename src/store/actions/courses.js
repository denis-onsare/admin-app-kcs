import {
  SET_COURSE,
  FETCHED_COURSES,
  FETCHED_COURSE,
  UPDATED_COURSE,
  DELETED_COURSE
} from "../types";
import api from "../../api";

export const coursePosted = course => ({
  type: SET_COURSE,
  course
});
export const coursesFetched = courses => ({
  type: FETCHED_COURSES,
  courses
});

export const courseUpdated = course => ({
  type: UPDATED_COURSE,
  course
});

export const courseFetched = course => ({
  type: FETCHED_COURSE,
  course
});
export const courseDeleted = courseId => ({
  type: DELETED_COURSE,
  courseId
});

export const postCourse = data => dispatch =>
  api.course.postCourse(data).then(course => dispatch(coursePosted(course)));

export const fetchCourses = () => dispatch =>
  api.course.fetchCourses().then(courses => dispatch(coursesFetched(courses)));

export const fetchCourseById = id => dispatch =>
  api.course.fetchCourse(id).then(course => dispatch(courseFetched(course)));

export const updateCourse = data => dispatch =>
  api.course.updateCourse(data).then(course => dispatch(courseUpdated(course)));

export const deleteCourse = id => dispatch => {
  const ok = window.confirm("Are you sure you want to delete this course?");
  if (ok === true) {
    return api.course.deleteCourse(id).then(() => dispatch(courseDeleted(id)));
  }
};
