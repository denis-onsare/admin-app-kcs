import {
  SET_JOURNAL,
  FETCHED_JOURNALS,
  FETCHED_JOURNAL,
  UPDATED_JOURNAL,
  DELETED_JOURNAL
} from "../types";
import api from "../../api";

export const journalPosted = journal => ({
  type: SET_JOURNAL,
  journal
});
export const journalsFetched = journals => ({
  type: FETCHED_JOURNALS,
  journals
});

export const journalUpdated = journal => ({
  type: UPDATED_JOURNAL,
  journal
});

export const journalFetched = journal => ({
  type: FETCHED_JOURNAL,
  journal
});
export const journalDeleted = journalId => ({
  type: DELETED_JOURNAL,
  journalId
});

export const postJournal = data => dispatch =>
  api.journal
    .postJournal(data)
    .then(journal => dispatch(journalPosted(journal)));

export const fetchJournals = () => dispatch =>
  api.journal
    .fetchJournals()
    .then(journals => dispatch(journalsFetched(journals)));

export const fetchJournalById = id => dispatch =>
  api.journal
    .fetchJournal(id)
    .then(journal => dispatch(journalFetched(journal)));

export const updateJournal = data => dispatch =>
  api.journal
    .updateJournal(data)
    .then(journal => dispatch(journalUpdated(journal)));

export const deleteJournal = id => dispatch =>{
      const ok = window.confirm("Are you sure you want to delete this journal?");

  if(ok === true){
  api.journal.deleteJournal(id).then(() => dispatch(journalDeleted(id)));

  }
}
