import { SET_LEADERSHIP, FETCHED_LEADERSHIP } from "../types";
import api from "../../api";

export const leadershipPosted = leader => ({
  type: SET_LEADERSHIP,
  leader
});
export const leadershipFetched = leaders => ({
  type: FETCHED_LEADERSHIP,
  leaders
});

export const postLeadership = data => dispatch =>
  api.leadership
    .postLeadership(data)
    .then(leader => dispatch(leadershipPosted(leader)));
export const fetchLeadership = () => dispatch =>
  api.leadership
    .fetchLeadership()
    .then(leaders => dispatch(leadershipFetched(leaders)));
