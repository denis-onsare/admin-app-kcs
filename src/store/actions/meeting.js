import {
  SET_MEETING,
  FETCHED_MEETINGS,
  UPDATED_MEETING,
  FETCHED_MEETING,
  DELETED_MEETING
} from "../types";
import api from "../../api";

export const meetingPosted = meeting => ({
  type: SET_MEETING,
  meeting
});
export const meetingsFetched = meetings => ({
  type: FETCHED_MEETINGS,
  meetings
});
export const meetingUpdated = meeting => ({
  type: UPDATED_MEETING,
  meeting
});
export const meetingFetched = meeting => ({
  type: FETCHED_MEETING,
  meeting
});
export const meetingDeleted = meetingId => ({
  type: DELETED_MEETING,
  meetingId
});

export const postMeeting = data => dispatch =>
  api.meeting
    .postMeeting(data)
    .then(meeting => dispatch(meetingPosted(meeting)));
export const fetchMeetings = () => dispatch =>
  api.meeting
    .fetchMeetings()
    .then(meetings => dispatch(meetingsFetched(meetings)));
export const fetchMeetingById = id => dispatch =>
  api.meeting
    .fetchMeeting(id)
    .then(meeting => dispatch(meetingFetched(meeting)));

export const updateMeeting = data => dispatch =>
  api.meeting
    .updateMeeting(data)
    .then(meeting => dispatch(meetingUpdated(meeting)));

export const deleteMeeting = id => dispatch =>{
    const ok = window.confirm("Are you sure you want to delete this meeting?");
if(ok === true){
  api.meeting.deleteMeeting(id).then(() => dispatch(meetingDeleted(id)));
}

}
