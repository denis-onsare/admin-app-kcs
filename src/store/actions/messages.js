import { FETCHED_MESSAGES } from "../types";
import api from "../../api";

export const messagesFetched = messages => ({
  type: FETCHED_MESSAGES,
  messages
});

export const fetchMessages = () => dispatch =>
  api.messages
    .fetchMessages()
    .then(messages => dispatch(messagesFetched(messages)));
