import {
  SET_PAST_ACTIVITY,
  FETCHED_PAST_ACTIVITY,
  UPDATED_ACTIVITY,
  FETCHED_ACTIVITY,
  DELETED_ACTIVITY
} from "../types";
import api from "../../api";

export const activityPosted = activity => ({
  type: SET_PAST_ACTIVITY,
  activity
});
export const activitiesFetched = activities => ({
  type: FETCHED_PAST_ACTIVITY,
  activities
});
export const activityUpdated = activity => ({
  type: UPDATED_ACTIVITY,
  activity
});
export const activityFetched = activity => ({
  type: FETCHED_ACTIVITY,
  activity
});
export const activityDeleted = activityId => ({
  type: DELETED_ACTIVITY,
  activityId
});

export const postActivity = data => dispatch =>
  api.activity.postActivity(data).then(activity => {
    dispatch(activityPosted(activity));
  });
export const fetchActivity = () => dispatch =>
  api.activity
    .fetchActivities()
    .then(activities => dispatch(activitiesFetched(activities)));
export const fetchActivityById = id => dispatch =>
  api.activity
    .fetchActivity(id)
    .then(activity => dispatch(activityFetched(activity)));

export const updateActivity = data => dispatch =>
  api.activity
    .updateActivity(data)
    .then(activity => dispatch(activityUpdated(activity)));

export const deleteActivity = id => dispatch =>{
    const ok = window.confirm("Are you sure you want to delete this activity?");

if(ok === true){
    api.activity.deleteActivity(id).then(() => dispatch(activityDeleted(id)));
  }
}
