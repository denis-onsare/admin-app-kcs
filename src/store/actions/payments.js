import { FETCHED_PAYMENTS } from "../types";
import api from "../../api";

export const paymentsFetched = payments => ({
  type: FETCHED_PAYMENTS,
  payments
});

export const fetchPayments = () => dispatch =>
  api.payments
    .fetchPayments()
    .then(payments => dispatch(paymentsFetched(payments)));
