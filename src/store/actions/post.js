import api from "../../api";
import {
  SET_POST,
  FETCHED_POSTS,
  FETCHED_POST,
  UPDATED_POST,
  DELETED_POST
} from "../types";

export const postPosted = post => ({
  type: SET_POST,
  post
});

export const postsFetched = posts => ({
  type: FETCHED_POSTS,
  posts
});
export const postUpdated = post => ({
  type: UPDATED_POST,
  post
});

export const postFetched = post => ({
  type: FETCHED_POST,
  post
});
export const postDeleted = postId => ({
  type: DELETED_POST,
  postId
});

export const createPost = data => dispatch =>
  api.posts.postPost(data).then(post => dispatch(postPosted(post)));

export const fetchPosts = () => dispatch =>
  api.posts.fetchPosts().then(posts => dispatch(postsFetched(posts)));

export const fetchPostById = id => dispatch =>
  api.posts.fetchPost(id).then(post => dispatch(postFetched(post)));

export const updatePost = data => dispatch =>
  api.posts.updatePost(data).then(post => dispatch(postUpdated(post)));

export const deletePost = id => dispatch =>
  api.posts.deletePost(id).then(() => dispatch(postDeleted(id)));
