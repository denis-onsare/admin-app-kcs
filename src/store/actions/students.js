import api from "../../api";
import { FETCHED_STUDENTS } from "../types";

export const studentsFetched = students => ({
  type: FETCHED_STUDENTS,
  students
});

export const fetchCourseStudents = () => dispatch =>
  api.students
    .fetchStudents()
    .then(students => dispatch(studentsFetched(students)));
