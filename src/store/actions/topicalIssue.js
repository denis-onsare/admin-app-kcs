import api from "../../api";
import {
  SET_TOPICAL_ISSUE,
  FETCHED_TOPICAL_ISSUES,
  FETCHED_TOPICAL_ISSUE,
  UPDATED_TOPICAL_ISSUE,
  DELETED_TOPICAL_ISSUE
} from "../types";

export const issuePosted = issue => ({
  type: SET_TOPICAL_ISSUE,
  issue
});

export const issuesFetched = issues => ({
  type: FETCHED_TOPICAL_ISSUES,
  issues
});

export const issueUpdated = issue => ({
  type: UPDATED_TOPICAL_ISSUE,
  issue
});

export const issueFetched = issue => ({
  type: FETCHED_TOPICAL_ISSUE,
  issue
});
export const issueDeleted = issueId => ({
  type: DELETED_TOPICAL_ISSUE,
  issueId
});

export const fetchIssues = () => dispatch =>
  api.topicalIssue
    .fetchIssues()
    .then(issues => dispatch(issuesFetched(issues)));

export const postTopicalIssue = data => dispatch =>
  api.topicalIssue.postIssue(data).then(issue => dispatch(issuePosted(issue)));

export const fetchIssueById = id => dispatch =>
  api.topicalIssue
    .fetchIssueById(id)
    .then(issue => dispatch(issueFetched(issue)));

export const updateIssue = data => dispatch =>
  api.topicalIssue
    .updateIssue(data)
    .then(issue => dispatch(issueUpdated(issue)));

export const deleteIssue = id => dispatch =>{
    const ok = window.confirm("Are you sure you want to delete this issue?");
  if(ok===true){
  api.topicalIssue.deleteIssue(id).then(() => dispatch(issueDeleted(id)));

  }
}
