import api from "../../api";
import { FETCHED_USERS, USER_UPDATED, USER_DELETED } from "../types";

export const userFetched = users => ({
  type: FETCHED_USERS,
  users
});

export const userDeleted = userId => ({
  type: USER_DELETED,
  userId
});
export const userUpdated = user => ({
  type: USER_UPDATED,
  user
});

export const fetchUsers = () => dispatch =>
  api.user.fetchUsers().then(users => dispatch(userFetched(users)));

export const updateUser = data => dispatch =>
  api.user.updateUser(data).then(user => dispatch(userUpdated(user)));
export const deleteUser = id => dispatch => {
  const ok = window.confirm("Are you sure you want to delete this user?");
  if (ok === true) {
    return api.user.deleteUser(id).then(() => dispatch(userDeleted(id)));
  }
};
