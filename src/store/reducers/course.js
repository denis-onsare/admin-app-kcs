import {
  SET_COURSE,
  FETCHED_COURSES,
  FETCHED_COURSE,
  UPDATED_COURSE,
  DELETED_COURSE
} from "../types";

export default function(state = [], action = {}) {
  switch (action.type) {
    case SET_COURSE:
      return [...state, action.course];
    case FETCHED_COURSES:
      return action.courses;
    case FETCHED_COURSE:
      const index = state.findIndex(item => item._id === action.course._id);
      if (index > -1) {
        return state.map(item => {
          if (item._id === action.course._id) return action.course;
          return item;
        });
      } else {
        return [...state, action.course];
      }
    case UPDATED_COURSE:
      return state.map(item => {
        if (item._id === action.course._id) return action.course;
        return item;
      });
    case DELETED_COURSE:
      return state.filter(item => item._id !== action.courseId);
    default:
      return state;
  }
}
