import {
  SET_JOURNAL,
  FETCHED_JOURNALS,
  FETCHED_JOURNAL,
  UPDATED_JOURNAL,
  DELETED_JOURNAL
} from "../types";

export default function(state = [], action = {}) {
  switch (action.type) {
    case SET_JOURNAL:
      return [...state, action.journal];
    case FETCHED_JOURNALS:
      return action.journals;
    case FETCHED_JOURNAL:
      const index = state.findIndex(item => item._id === action.journal._id);
      if (index > -1) {
        return state.map(item => {
          if (item._id === action.journal._id) return action.journal;
          return item;
        });
      } else {
        return [...state, action.journal];
      }
    case UPDATED_JOURNAL:
      return state.map(item => {
        if (item._id === action.journal._id) return action.journal;
        return item;
      });
    case DELETED_JOURNAL:
      return state.filter(item => item._id !== action.journalId);
    default:
      return state;
  }
}
