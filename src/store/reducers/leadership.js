import { SET_LEADERSHIP, FETCHED_LEADERSHIP } from "../types";

export default function(state = [], action = {}) {
  switch (action.type) {
    case SET_LEADERSHIP:
      return [...state, action.leader];
    case FETCHED_LEADERSHIP:
      return action.leaders;
    default:
      return state;
  }
}
