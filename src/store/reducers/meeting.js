import {
  SET_MEETING,
  FETCHED_MEETINGS,
  FETCHED_MEETING,
  UPDATED_MEETING,
  DELETED_MEETING
} from "../types";

export default function(state = [], action = {}) {
  switch (action.type) {
    case SET_MEETING:
      return [...state, action.meeting];
    case FETCHED_MEETINGS:
      return action.meetings;
    case FETCHED_MEETING:
      const index = state.findIndex(item => item._id === action.meeting._id);
      if (index > -1) {
        return state.map(item => {
          if (item._id === action.meeting._id) return action.meeting;
          return item;
        });
      } else {
        return [...state, action.meeting];
      }
    case UPDATED_MEETING:
      return state.map(item => {
        if (item._id === action.meeting._id) return action.meeting;
        return item;
      });
    case DELETED_MEETING:
      return state.filter(item => item._id !== action.meetingId);
    default:
      return state;
  }
}
