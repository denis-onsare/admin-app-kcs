import { FETCHED_MESSAGES } from "../types";

export default function(state = [], action = {}) {
  switch (action.type) {
    case FETCHED_MESSAGES:
      return action.messages;

    default:
      return state;
  }
}
