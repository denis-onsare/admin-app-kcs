import {
  SET_PAST_ACTIVITY,
  FETCHED_PAST_ACTIVITY,
  UPDATED_ACTIVITY,
  FETCHED_ACTIVITY,
  DELETED_ACTIVITY
} from "../types";

export default function(state = [], action = {}) {
  switch (action.type) {
    case SET_PAST_ACTIVITY:
      return [...state, action.activity];
    case FETCHED_PAST_ACTIVITY:
      return action.activities;
    case FETCHED_ACTIVITY:
      const index = state.findIndex(item => item._id === action.activity._id);
      if (index > -1) {
        return state.map(item => {
          if (item._id === action.activity._id) return action.activity;
          return item;
        });
      } else {
        return [...state, action.activity];
      }
    case UPDATED_ACTIVITY:
     return state.map(item => {
        if (item._id === action.activity._id) return action.activity;
        return item;
      });
    case DELETED_ACTIVITY:
      return state.filter(item => item._id !== action.activityId);
    default:
      return state;
  }
}
