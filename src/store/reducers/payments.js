import { FETCHED_PAYMENTS } from "../types";

export default function(state = [], action = {}) {
  switch (action.type) {
    case FETCHED_PAYMENTS:
      return action.payments;

    default:
      return state;
  }
}
