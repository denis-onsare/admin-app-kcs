import {
  SET_POST,
  FETCHED_POSTS,
  FETCHED_POST,
  UPDATED_POST,
  DELETED_POST
} from "../types";

export default function(state = [], action = {}) {
  switch (action.type) {
    case SET_POST:
      return [...state, action.post];
    case FETCHED_POSTS:
      return action.posts;
    case FETCHED_POST:
      const index = state.findIndex(item => item._id === action.post._id);
      if (index > -1) {
        return state.map(item => {
          if (item._id === action.post._id) return action.post;
          return item;
        });
      } else {
        return [...state, action.post];
      }
    case UPDATED_POST:
      state.map(item => {
        if (item._id === action.post._id) {
          return action.post;
        }
        return item;
      });
      break;
    case DELETED_POST:
      return state.filter(item => item._id !== action.postId);

    default:
      return state;
  }
}
