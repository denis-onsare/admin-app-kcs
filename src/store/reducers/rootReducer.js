import { combineReducers } from "redux";
import user from "./user";
import meeting from "./meeting";
import activity from "./pastActivity";
import journal from "./journal";
import topicalIssue from "./topicalIssue";
import post from "./post";
import users from "./users";
import leadership from "./leadership";
import messages from "./messages";
import payments from "./payments";
import course from "./course";
import students from "./students";

export default combineReducers({
  user,
  meeting,
  activity,
  journal,
  topicalIssue,
  post,
  users,
  leadership,
  messages,
  payments,
  course,
  students
});
