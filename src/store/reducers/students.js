import { FETCHED_STUDENTS } from "../types";

export default function(state = [], action = {}) {
  switch (action.type) {
    case FETCHED_STUDENTS:
      return action.students;

    default:
      return state;
  }
}
