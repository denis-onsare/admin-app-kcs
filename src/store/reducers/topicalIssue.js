import {
  SET_TOPICAL_ISSUE,
  FETCHED_TOPICAL_ISSUES,
  FETCHED_TOPICAL_ISSUE,
  UPDATED_TOPICAL_ISSUE,
  DELETED_TOPICAL_ISSUE
} from "../types";

export default function(state = [], action = {}) {
  switch (action.type) {
    case SET_TOPICAL_ISSUE:
      return [...state, action.issue];
    case FETCHED_TOPICAL_ISSUES:
      return action.issues;
    case FETCHED_TOPICAL_ISSUE:
      const index = state.findIndex(item => item._id === action.issue._id);
      if (index > -1) {
        return state.map(item => {
          if (item._id === action.issue._id) return action.issue;
          return item;
        });
      } else {
        return [...state, action.issue];
      }
    case UPDATED_TOPICAL_ISSUE:
     return state.map(item => {
        if (item._id === action.issue._id) return action.issue;
        return item;
      });
    case DELETED_TOPICAL_ISSUE:
      return state.filter(item => item._id !== action.issueId);
    default:
      return state;
  }
}
