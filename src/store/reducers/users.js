import { FETCHED_USERS, USER_DELETED, USER_UPDATED } from "../types";

export default function(state = [], action = {}) {
  switch (action.type) {
    case FETCHED_USERS:
      return action.users;
    case USER_UPDATED:
      return state.map(item => {
        if (item._id === action.user._id) return action.user;
        return item;
      });
    case USER_DELETED:
      return state.filter(item => item._id !== action.userId);
    default:
      return state;
  }
}
