import sha1 from "sha1";
import superagent from "superagent";

export const fileUpload = files => {
  console.log("file uploaded: ");
  const image = files[0];
  // const cloudname = "dn4vattmg";
  const url = "https://api.cloudinary.com/v1_1/dn4vattmg/image/upload";
  const timestamp = Date.now() / 1000;
  const uploadPreset = "eqsrekjy";
  const paramsStr =
    "timestamp=" +
    timestamp +
    "&upload_preset=" +
    uploadPreset +
    "PNz72H39iHBrNl5QtU4hBMsjQb8";
  const signature = sha1(paramsStr);
  const params = {
    api_key: "883813167662772",
    timestamp: timestamp,
    upload_preset: uploadPreset,
    signature: signature
  };
  let uploadRequest = superagent.post(url);
  uploadRequest.attach("file", image);

  Object.keys(params).forEach(key => {
    uploadRequest.field(key, params[key]);
  });

  uploadRequest.end((err, resp) => {
    if (err) {
      alert(err, null);
      return;
    }
    console.log("UPLOAD COMPLETE:" + JSON.stringify(resp.body));
    const uploaded = resp.body;
    let images = Object.assign([], this.state.data.image);
    images.push(uploaded);
    this.setState({
      data: { image: images }
    });
  });
};
