import React from "react";
import { Container, Col, Row, Card, CardBody, CardHeader } from "reactstrap";
import { connect } from "react-redux";
import Moment from "react-moment";
import { fetchPosts } from "../../store/actions/post";

class Comments extends React.Component {
  state = {};

  componentDidMount() {
    this.props.fetchPosts();
  }

  render() {
    const { post } = this.props;
    return (
      <Container>
        <Row className="pt40 pb40">
          <Col sm="6">
            <Card>
              <CardHeader> Comments </CardHeader>
              <CardBody>
                <div>
                  {post.comments
                    ? post.comments.map(item => (
                        <div key={item._id}>
                          <div className="avatar">
                            <img
                              className="img-avatar"
                              width="100%"
                              src="/img/avatar.png"
                              alt="post avatar"
                            />
                          </div>

                          <div>{item.comment}</div>
                          <small>
                            created: <Moment fromNow>{post.updatedAt}</Moment>
                            &nbsp; by: {item.email.slice(0, 5)}
                          </small>
                        </div>
                      ))
                    : "no comments"}
                </div>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
    );
  }
}

function mapStateToProps(state) {
  return {
    post: state.post
  };
}

export default connect(
  mapStateToProps,
  { fetchPosts }
)(Comments);
