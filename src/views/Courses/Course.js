import React from "react";
import {
  Container,
  Row,
  Col,
  Card,
  // CardImg,
  CardBody
  // CardTitle,
  // CardText,
  // CardSubtitle,
  // Button
} from "reactstrap";
// import { Link } from "react-router-dom";
import CourseWalkThrough from "./courseWalkThrough/index";

import { connect } from "react-redux";
import { fetchCourseById } from "../../store/actions/courses";
// import Moment from "react-moment";

class Course extends React.Component {
  state = {
    course: {
      _id: this.props.course ? this.props.course._id : null,
      description: this.props.course ? this.props.course.description : "",
      image: this.props.course ? this.props.course.image : "",
      title: this.props.course ? this.props.course.title : "",
      author: this.props.course ? this.props.course.author : "",
      content: this.props.content ? this.props.course.content : ""
    }
  };
  componentDidMount = () => {
    const { match } = this.props;
    if (match.params._id) {
      this.props.fetchCourseById(match.params._id);
    }
  };

  componentWillReceiveProps = nextProps => {
    this.setState({
      course: {
        _id: nextProps.course._id,
        title: nextProps.course.title,
        image: nextProps.course.image,
        description: nextProps.course.description,
        author: nextProps.course.author,
        content: nextProps.course.content
      }
    });
  };

  render() {
    const { course } = this.state;

    return (
      <Container>
        <Row className="mt40 mb40">
          <Col>
            <Card>
              {/* <CardImg
                top
                width="100%"
                height="350px"
                src={course.image}
                alt={course.title}
              /> */}
              <CardBody>
                <h1>{course.title}</h1>
                <h3>Instructor: {course.author}</h3>
                {/* <CardTitle>{course.keywords}</CardTitle> */}
                {/* <CardSubtitle>
                  Date <Moment format="YYYY/MM/DD">{course.created}</Moment>
                </CardSubtitle>

                <div dangerouslySetInnerHTML={{ __html: course.content[0] }} />

                <div className="float-right">
                
                  <Link to={`/courses/edit/${course._id}`}>
                    <Button color="success">Edit</Button>
                  </Link>
                </div> */}
                <CourseWalkThrough course={course} />
              </CardBody>
            </Card>
          </Col>
        </Row>
        {/* <Row className="mt40 mb40">
          <Col sm={{ size: "8", offset: "2" }}>
            <Card>
              <CardHeader>Comments</CardHeader>
              <CardBody>no comment</CardBody>
            </Card>
          </Col>
        </Row> */}
      </Container>
    );
  }
}
function mapStateToProps(state, props) {
  const { match } = props;
  if (match.params._id) {
    return {
      course: state.course.find(item => item._id === match.params._id)
    };
  }

  return { course: null };
}

export default connect(mapStateToProps, { fetchCourseById })(Course);
