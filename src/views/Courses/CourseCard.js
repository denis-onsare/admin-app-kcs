import React from "react";
import { Col, Button, Card, CardBody } from "reactstrap";
import { Link } from "react-router-dom";

const CourseCard = ({ course, deleteCourse }) => (
  <Col sm="4">
    <Card>
      <img width="100%" src={course.image} alt="" />
      <CardBody>
        <h2>{course.title}</h2>

        <div dangerouslySetInnerHTML={{ __html: course.description }} />

        <Link to={`/courses/statistics/${course._id}`}>
          <Button color="warning" className="mr10">
            Stats
          </Button>
        </Link>
        <Link to={`/courses/edit/${course._id}`}>
          <Button className="mr10">
            <span
              style={{
                color: "red",
                cursor: "pointer",
                fontSize: "25px"
              }}
            >
              <i className="fa fa-pencil"></i>
            </span>
          </Button>
        </Link>

        <Button className="mr10" onClick={() => deleteCourse(course._id)}>
          <span
            style={{
              color: "red",
              cursor: "pointer",
              fontSize: "25px"
            }}
          >
            <i className="fa fa-trash"></i>
          </span>
        </Button>
      </CardBody>
    </Card>
  </Col>
);

export default CourseCard;
