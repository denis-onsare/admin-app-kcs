import React from "react";
import CourseCard from "./CourseCard";
import EmptyPage from "../../components/partials/EmptyPage";
import { Row } from "reactstrap";

class CourseList extends React.Component {
  state = {};

  render() {
    const courseList = (
      <Row>
        {this.props.courses.map(course => (
          <CourseCard
            course={course}
            key={course._id}
            deleteCourse={this.props.deleteCourse}
          />
        ))}
      </Row>
    );
    return (
      <div>
        {this.props.courses.length === 0 ? (
          <EmptyPage message="courses" />
        ) : (
          courseList
        )}
      </div>
    );
  }
}

export default CourseList;
