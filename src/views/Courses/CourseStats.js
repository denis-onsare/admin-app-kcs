import React, { Component } from "react";
import Widget04 from "../Widgets/Widget04";
import { connect } from "react-redux";
import { fetchCourseById } from "../../store/actions/courses";
// import { Link } from "react-router-dom";
import Moment from "react-moment";
import {
  Card,
  CardBody,
  // CardFooter,
  CardHeader,
  // CardTitle,
  Col,
  Progress,
  CardGroup,
  Row,
  Table
} from "reactstrap";

class Stats extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.onRadioBtnClick = this.onRadioBtnClick.bind(this);

    this.state = {
      dropdownOpen: false,
      radioSelected: 2
    };
  }

  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    });
  }

  onRadioBtnClick(radioSelected) {
    this.setState({
      radioSelected: radioSelected
    });
  }

  componentDidMount = () => {
    const id = this.props.match.params._id;
    this.props.fetchCourseById(id);
  };

  render() {
    const { students } = this.props;
    // console.log(students.map(item => item.students.map(item => item.user)));
    const participants = [];
    students.map(item =>
      item.students.map(item => participants.push(item.user))
    );
    console.log(participants);

    return (
      <div className="animated fadeIn">
        <Row>
          <Col>
            <CardGroup className="mb-4">
              <Widget04
                icon="icon-people"
                color="info"
                header={students.map(item => item.students.length)}
                value="5"
              >
                Enrolled Users
              </Widget04>
              <Widget04
                icon="icon-user-follow"
                color="success"
                header="0"
                value="5"
              >
                Certificates Issued
              </Widget04>
            </CardGroup>
          </Col>
        </Row>
        <Row>
          <Col>
            <Card>
              <CardHeader>Participants</CardHeader>
              <CardBody>
                <br />
                <Table
                  hover
                  responsive
                  className="table-outline mb-0 d-none d-sm-table"
                >
                  <thead className="thead-light">
                    <tr>
                      <th>
                        <i className="icon-people" />
                      </th>
                      <th>Name</th>
                      <th>Email</th>

                      <th>ID</th>
                    </tr>
                  </thead>
                  <tbody>
                    {participants.map((user, i) => (
                      <tr key={i}>
                        <td>
                          <div className="avatar">
                            <img
                              src={user && user.avatar ? user.avatar : "g"}
                              className="img-avatar"
                              alt={user && user.firstName ? user.firstName : ""}
                            />
                            <span className="avatar-status badge-success" />
                          </div>
                        </td>
                        <td>
                          <p>
                            {user && user.firstName ? user.firstName : ""}{" "}
                            {user && user.lastName ? user.lastName : ""}
                          </p>
                        </td>
                        <td>
                          <p>{user && user.email ? user.email : ""}</p>
                        </td>

                        <td>
                          <div>{user && user._id ? user._id : ""}</div>
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </Table>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

function mapStateToProps(state, props) {
  const { match } = props;
  return {
    students: state.course.filter(item => item._id === match.params._id)
  };
}

export default connect(mapStateToProps, { fetchCourseById })(Stats);
