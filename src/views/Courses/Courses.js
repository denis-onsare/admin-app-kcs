import React from "react";
import { connect } from "react-redux";
import { Container, Card, CardHeader, CardBody } from "reactstrap";

import { fetchCourses, deleteCourse } from "../../store/actions/courses";
import CourseList from "./CourseList";

class Courses extends React.Component {
  componentDidMount() {
    this.props.fetchCourses();
  }

  render() {
    return (
      <Container>
        <Card>
          <CardHeader>All Courses</CardHeader>
          <CardBody>
            <CourseList
              courses={this.props.courses}
              deleteCourse={this.props.deleteCourse}
            />
          </CardBody>
        </Card>
      </Container>
    );
  }
}

function mapStateToProps(state) {
  return {
    courses: state.course
  };
}

export default connect(mapStateToProps, { fetchCourses, deleteCourse })(
  Courses
);
