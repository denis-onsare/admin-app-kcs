import React from "react";

import {
  Container,
  Row,
  Col,
  Form,
  FormGroup,
  Input,
  Label,
  Card,
  CardHeader,
  CardBody,
  Button,
  FormFeedback
} from "reactstrap";

import { connect } from "react-redux";
import {
  postCourse,
  fetchCourseById,
  updateCourse
} from "../../store/actions/courses";
import LoadingState from "../../components/states/LoadingState";
// import Dropzone from "react-dropzone";

class CreateCourse extends React.Component {
  state = {
    data: {
      _id: this.props.course ? this.props.course._id : null,
      description: this.props.course ? this.props.course.description : "",
      image: this.props.course ? this.props.course.image : "",
      title: this.props.course ? this.props.course.title : "",
      author: this.props.course ? this.props.course.author : ""
    },
    errors: {},
    loading: false
  };
  componentDidMount = () => {
    const { match } = this.props;
    if (match.params._id) {
      this.props.fetchCourseById(match.params._id);
    }
  };

  componentWillReceiveProps = nextProps => {
    this.setState({
      data: {
        _id: nextProps.course._id,
        title: nextProps.course.title,
        image: nextProps.course.image,
        description: nextProps.course.description,
        author: nextProps.course.author
      }
    });
  };

  onSubmit = e => {
    e.preventDefault();

    const errors = this.validate(this.state.data);
    const data = this.state.data;
    let bodyFormData = new FormData();
    if (data._id) bodyFormData.set("_id", data._id);
    bodyFormData.set("description", data.description);
    bodyFormData.set("author", data.author);
    bodyFormData.set("title", data.title);
    bodyFormData.append("image", this.state.image);

    this.setState({ errors });
    if (Object.keys(errors).length === 0) {
      this.setState({ loading: true });
      if (data._id) {
        this.props
          .updateCourse(bodyFormData)
          .then(() => this.props.history.push("/courses"))
          .catch(err =>
            this.setState({ errors: err.response.data, loading: false })
          );
      } else {
        this.props
          .postCourse(bodyFormData)
          .then(() => this.props.history.push("/courses"))
          .catch(err =>
            this.setState({ errors: err.response.data, loading: false })
          );
      }
    }
  };

  onChange = e => {
    this.setState({
      data: { ...this.state.data, [e.target.name]: e.target.value }
    });
  };

  handleFileUpload = e => {
    let image_as_base64 = URL.createObjectURL(e.target.files[0]);
    this.setState({ image: e.target.files[0], image_as_base64 });
  };

  validate = data => {
    const errors = {};

    if (!data.title) errors.title = "title is required";
    if (!data.author) errors.author = "author is required";
    // if (!data.image) errors.image = "image is required";
    if (!data.description) errors.description = "description is required";
    return errors;
  };

  render() {
    const { data, errors, loading } = this.state;

    return (
      <div>
        <Container>
          <Row>
            <Col>
              <Card>
                <CardHeader>New Course</CardHeader>

                <CardBody>
                  {loading && <LoadingState message="loading..." />}
                  <Form onSubmit={this.onSubmit} encType="multipart/form-data">
                    {errors.error ? (
                      <div className="alert alert-danger">{errors.error}</div>
                    ) : (
                      ""
                    )}
                    <FormGroup row>
                      <div
                        style={{
                          border: "2px dotted #ccc",
                          height: "250px",
                          width: "500px",
                          display: "flex",
                          justifyContent: "center",
                          alignItems: "center"
                        }}
                      >
                        {this.state.image ? (
                          <img
                            width="100%"
                            src={this.state.image_as_base64}
                            alt=""
                          />
                        ) : (
                          <Label htmlFor="course-poster" sm={3}>
                            <span className="btn btn-success">
                              <i className="fa fa-upload"></i> Upload Poster
                            </span>
                          </Label>
                        )}
                      </div>
                      <Input
                        accept="image/*"
                        id="course-poster"
                        type="file"
                        name="image"
                        onChange={this.handleFileUpload}
                        // invalid={!!errors.image}
                        style={{ display: "none" }}
                      />
                    </FormGroup>
                    <FormGroup>
                      <Label>Title</Label>
                      <Input
                        type="text"
                        placeholder="Journal title"
                        name="title"
                        onChange={this.onChange}
                        value={data.title}
                        invalid={!!errors.title}
                      />
                      <FormFeedback>{errors.title}</FormFeedback>
                    </FormGroup>
                    <FormGroup>
                      <Label>Author</Label>
                      <Input
                        type="text"
                        placeholder="Authors Name"
                        name="author"
                        onChange={this.onChange}
                        value={data.author}
                        invalid={!!errors.author}
                      />
                      <FormFeedback>{errors.author}</FormFeedback>
                    </FormGroup>

                    <FormGroup>
                      <Label>description</Label>
                      <Input
                        type="textarea"
                        style={{ height: 200 }}
                        placeholder="description"
                        name="description"
                        onChange={this.onChange}
                        value={data.description}
                        invalid={!!errors.description}
                      />
                      <FormFeedback>{errors.description}</FormFeedback>
                    </FormGroup>

                    <Button>Submit</Button>
                  </Form>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

function mapStateToProps(state, props) {
  const { match } = props;
  if (match.params._id) {
    return {
      course: state.course.find(item => item._id === match.params._id)
    };
  }

  return { course: null };
}

export default connect(mapStateToProps, {
  postCourse,
  fetchCourseById,
  updateCourse
})(CreateCourse);
