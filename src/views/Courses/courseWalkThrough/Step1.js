import React, { Component } from "react";
import { Button } from "reactstrap";

class Step1 extends Component {
  state = {
    errors: {}
  };
  continue = () => {
    const { step, nextStep } = this.props;

    if (true) {
      nextStep(step);
    }
  };

  render() {
    return (
      <React.Fragment>
        <div
          dangerouslySetInnerHTML={{ __html: this.props.course.content[0] }}
        />

        <Button onClick={this.continue} className="btn-style-two">
          continue <i className="fa fa-arrow-right"></i>
        </Button>
      </React.Fragment>
    );
  }
}

export default Step1;
