import React, { Component } from "react";
import { Button } from "reactstrap";

class Step3 extends Component {
  state = {
    errors: {}
  };

  continue = () => {
    const { step, nextStep } = this.props;

    if (true) {
      return nextStep(step);
    }
  };

  goBack = () => {
    const { step } = this.props;
    this.props.prevStep(step);
  };
  render() {
    return (
      <React.Fragment>
        <div
          dangerouslySetInnerHTML={{ __html: this.props.course.content[2] }}
        />
        <div style={{ display: "flex", justifyContent: "space-between" }}>
          <Button onClick={this.goBack} className="btn-style-two">
            <i className="fa fa-arrow-left"></i> Back
          </Button>
          <Button onClick={this.continue} className="btn-style-two">
            Continue <i className="fa fa-arrow-right"></i>
          </Button>
        </div>
      </React.Fragment>
    );
  }
}

export default Step3;
