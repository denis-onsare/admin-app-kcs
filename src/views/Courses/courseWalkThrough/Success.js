import React from "react";
import successgif from "../../../img/success.gif";
import { Button } from "reactstrap";
import { Link } from "react-router-dom";

const Success = () => {
  return (
    <div
      style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "column"
      }}
    >
      <React.Fragment>
        <img
          style={{ borderRadius: "5px", width: "30%" }}
          src={successgif}
          alt="success"
        />
        <p style={{ color: "green" }}>Hurray! Course Completed</p>
      </React.Fragment>
      <Link to="/courses">
        <Button className="btn-style-two">Back to Courses</Button>
      </Link>
    </div>
  );
};

export default Success;
