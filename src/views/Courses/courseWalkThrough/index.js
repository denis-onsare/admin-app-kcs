import React, { Component } from "react";
import Success from "./Success";
import Step1 from "./Step1";
import Step2 from "./Step2";
import Step3 from "./Step3";

class Registration extends Component {
  state = {
    step: 1,
    errors: {}
  };

  nextStep = () => {
    const { step } = this.state;
    this.setState({ step: step + 1 });
  };
  prevStep = () => {
    const { step } = this.state;
    this.setState({ step: step - 1 });
  };

  onSubmit = data => this.props.submit(data);

  render() {
    const { step } = this.state;
    const { course } = this.props;
    let content = course.content;
    for (let i = 0; i < content.length; i++) {}

    switch (step) {
      case 1:
        return <Step1 nextStep={this.nextStep} step={step} course={course} />;
      case 2:
        return (
          <Step2
            nextStep={this.nextStep}
            step={step}
            prevStep={this.prevStep}
            course={course}
          />
        );
      case 3:
        return (
          <Step3
            nextStep={this.nextStep}
            step={step}
            prevStep={this.prevStep}
            course={course}
          />
        );

      case 4:
        return <Success />;

      default:
        break;
    }
  }
}

export default Registration;
