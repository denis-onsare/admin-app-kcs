import React, { Component } from "react";
import {
  Button,
  Card,
  CardBody,
  CardHeader,
  FormGroup,
  Label
} from "reactstrap";
import CKEditor from "@ckeditor/ckeditor5-react";
import ClassicEditor from "@ckeditor/ckeditor5-build-classic";

class MemberShipDetailsForm extends Component {
  continue = () => {
    const { step, data, nextStep } = this.props;
    const errors = this.validate(data);
    this.setState({ errors });
    if (Object.keys(errors).length === 0) {
      return nextStep(step);
    }
  };
  validate = data => {
    const errors = {};

    return errors;
  };
  goBack = () => {
    const { step } = this.props;
    this.props.prevStep(step);
  };
  render() {
    const { content, onEditorChange, addSection, deleteSection } = this.props;
    console.log(content);
    return (
      <React.Fragment>
        <Card>
          <CardHeader>Main Content</CardHeader>
          <CardBody style={{ minHeight: "50vh" }}>
            <div style={{ display: "flex", justifyContent: "space-between" }}>
              <Button onClick={this.goBack} className="btn-style-two">
                <i className="fa fa-arrow-left"></i> Back
              </Button>
              <Button onClick={this.continue} className="btn-style-two">
                Continue <i className="fa fa-arrow-right"></i>
              </Button>
            </div>
            <Button
              color="warning"
              style={{
                position: "fixed",
                top: "60px",
                right: "50px",
                zIndex: 1000
              }}
              onClick={addSection}
            >
              <i className="fa fa-plus"></i>Add Section
            </Button>
            {content.map((section, index) => {
              return (
                <div
                  key={index}
                  style={{
                    background: "#ccc",
                    padding: "15px",
                    borderRadius: "15px",
                    margin: "25px 25px"
                  }}
                >
                  <FormGroup>
                    <Label>Section</Label>
                    <CKEditor
                      editor={ClassicEditor}
                      data={section}
                      config={{
                        ckfinder: {
                          // Upload the images to the server using the CKFinder QuickUpload command.
                          uploadUrl:
                            "https://kenyachemicalsociety.org/app1/api/courses/media"
                        }
                      }}
                      onInit={editor => {
                        // You can store the "editor" and use when it is needed.
                        console.log("Editor is ready to use!", editor);
                      }}
                      onChange={(e, editor) => {
                        const data = editor.getData();
                        onEditorChange(data, index);
                      }}
                    />
                  </FormGroup>
                  <Button color="danger" onClick={() => deleteSection(index)}>
                    remove
                  </Button>
                </div>
              );
            })}

            <div style={{ display: "flex", justifyContent: "space-between" }}>
              <Button onClick={this.goBack} className="btn-style-two">
                <i className="fa fa-arrow-left"></i> Back
              </Button>
              <Button onClick={this.continue} className="btn-style-two">
                Continue <i className="fa fa-arrow-right"></i>
              </Button>
            </div>
          </CardBody>
        </Card>
      </React.Fragment>
    );
  }
}

export default MemberShipDetailsForm;
