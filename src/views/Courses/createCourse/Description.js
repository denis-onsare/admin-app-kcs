import React, { Component } from "react";
import {
  Button,
  FormGroup,
  Input,
  Label,
  Card,
  CardBody,
  CardHeader
} from "reactstrap";
import CKEditor from "@ckeditor/ckeditor5-react";
import ClassicEditor from "@ckeditor/ckeditor5-build-classic";

class AccountDetailsForm extends Component {
  state = {
    errors: {}
  };

  continue = () => {
    const { step, data, nextStep } = this.props;
    const errors = this.validate(data);
    this.setState({ errors });
    if (Object.keys(errors).length === 0) {
      nextStep(step);
    }
  };
  validate = data => {
    const errors = {};
    if (!data.description)
      errors.description = "Course description is required";
    return errors;
  };
  goBack = () => {
    const { step } = this.props;
    this.props.prevStep(step);
  };
  render() {
    const {
      data,

      handleFileUpload,
      image,
      onChangeDescription
    } = this.props;
    const { errors } = this.state;
    console.log(image);
    return (
      <React.Fragment>
        <Card>
          <CardHeader>Course Description</CardHeader>
          <CardBody>
            <div style={{ display: "flex", justifyContent: "space-between" }}>
              <Button onClick={this.goBack} className="btn-style-two">
                <i className="fa fa-arrow-left"></i> Back
              </Button>
              <Button onClick={this.continue} className="btn-style-two">
                Continue <i className="fa fa-arrow-right"></i>
              </Button>
            </div>
            <Label>Course Poster</Label>
            <FormGroup row>
              <div
                style={{
                  border: "2px dotted #ccc",
                  height: "250px",
                  marginLeft: "15px",
                  width: "500px",
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center"
                }}
              >
                {this.props.image ? (
                  <img width="100%" src={image} alt="" />
                ) : (
                  <div>
                    <img width="100%" src={data.image} alt="" />
                    <Label
                      htmlFor="course-poster"
                      className="float-right"
                      sm={4}
                    >
                      <span className="btn btn-success">
                        <i className="fa fa-upload"></i> <br />
                        Upload Poster
                      </span>
                    </Label>
                  </div>
                )}
              </div>
              <Input
                accept="image/*"
                id="course-poster"
                type="file"
                name="image"
                onChange={handleFileUpload}
                // invalid={!!errors.image}
                style={{ display: "none" }}
              />
            </FormGroup>

            <FormGroup>
              <Label>Description</Label>

              <CKEditor
                editor={ClassicEditor}
                data={data.description}
                config={{
                  ckfinder: {
                    // Upload the images to the server using the CKFinder QuickUpload command.
                    uploadUrl:
                      "https://kenyachemicalsociety.org/app1/api/courses/media"
                  }
                }}
                onInit={editor => {
                  // You can store the "editor" and use when it is needed.
                  console.log("Editor is ready to use!", editor);
                }}
                onChange={(e, editor) => {
                  const data = editor.getData();
                  // onEditorCertChange(data, index);
                  onChangeDescription(data);
                }}
              />
              <p style={{ color: "red" }}>{errors.description}</p>
            </FormGroup>
            <div style={{ display: "flex", justifyContent: "space-between" }}>
              <Button onClick={this.goBack} className="btn-style-two">
                <i className="fa fa-arrow-left"></i> Back
              </Button>
              <Button onClick={this.continue} className="btn-style-two">
                Continue <i className="fa fa-arrow-right"></i>
              </Button>
            </div>
          </CardBody>
        </Card>
      </React.Fragment>
    );
  }
}

export default AccountDetailsForm;
