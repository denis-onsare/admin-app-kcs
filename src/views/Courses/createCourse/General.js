import React, { Component } from "react";
import {
  Button,
  FormGroup,
  Input,
  Label,
  FormFeedback,
  Card,
  CardBody,
  CardHeader
} from "reactstrap";
import CKEditor from "@ckeditor/ckeditor5-react";
import ClassicEditor from "@ckeditor/ckeditor5-build-classic";

class General extends Component {
  state = {
    errors: {}
  };
  continue = () => {
    const { step, data, nextStep } = this.props;
    const errors = this.validate(data);
    this.setState({ errors });
    if (Object.keys(errors).length === 0) {
      nextStep(step);
    }
  };
  validate = data => {
    const errors = {};
    if (!data.title) errors.title = "Name of course is requred";
    if (!data.author) errors.author = "Instructor(s) id required";
    return errors;
  };

  render() {
    const { data, onChange, onChangeSkills } = this.props;
    const { errors } = this.state;
    console.log(errors);
    return (
      <React.Fragment>
        <Card>
          <CardHeader>General Course Information</CardHeader>
          <CardBody>
            <FormGroup>
              <Label>Name of the Course</Label>
              <Input
                type="text"
                placeholder="course name"
                name="title"
                onChange={onChange}
                value={data.title}
                invalid={!!errors.title}
              />
              <FormFeedback>{errors.title}</FormFeedback>
            </FormGroup>
            <FormGroup>
              <Label>Instructor(s)</Label>
              <Input
                type="text"
                placeholder="names of instructors, comma separated"
                name="author"
                onChange={onChange}
                value={data.author}
                invalid={!!errors.author}
              />
              <FormFeedback>{errors.author}</FormFeedback>
            </FormGroup>
            <FormGroup>
              <Label>
                What will the student learn? A list of skills to be gained by
                taking this course
              </Label>
              <CKEditor
                editor={ClassicEditor}
                data={data.skills}
                config={{
                  ckfinder: {
                    // Upload the images to the server using the CKFinder QuickUpload command.
                    uploadUrl:
                      "https://kenyachemicalsociety.org/app1/api/courses/media"
                  }
                }}
                onInit={editor => {
                  // You can store the "editor" and use when it is needed.
                  console.log("Editor is ready to use!", editor);
                }}
                onChange={(e, editor) => {
                  const data = editor.getData();
                  // onEditorCertChange(data, index);
                  onChangeSkills(data);
                }}
              />
            </FormGroup>

            <Button onClick={this.continue} className="btn-style-two">
              continue <i className="fa fa-arrow-right"></i>
            </Button>
          </CardBody>
        </Card>
      </React.Fragment>
    );
  }
}

export default General;
