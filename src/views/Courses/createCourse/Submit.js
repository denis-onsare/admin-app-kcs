import React, { Component } from "react";
import {
  Button,
  Card,
  CardBody,
  CardHeader,
  Label,
  FormGroup,
  Input
} from "reactstrap";
import LoadingState from "../../../components/states/LoadingState";
import CKEditor from "@ckeditor/ckeditor5-react";
import ClassicEditor from "@ckeditor/ckeditor5-build-classic";

class Submit extends Component {
  state = {
    loading: false,
    errors: {}
  };
  next = data => {
    const { step, nextStep } = this.props;
    this.setState({ loading: true });
    // nextStep(step);
    this.props
      .onSubmit(data)
      .then(() => nextStep(step))
      .catch(err =>
        this.setState({ errors: err.response.data, loading: false })
      );
  };
  goBack = () => {
    const { step } = this.props;
    this.props.prevStep(step);
  };
  render() {
    const { data, handleCertUpload, image, certificate } = this.props;
    const { errors, loading } = this.state;
    return (
      <React.Fragment>
        <Card>
          <CardHeader>Publish Course</CardHeader>
          <CardBody>
            {loading && <LoadingState message="loading..." />}

            {errors.error ? (
              <div className="alert alert-danger">{errors.error}</div>
            ) : (
              ""
            )}
            <Label>Course Certificate (optional)</Label>
            <FormGroup row>
              <div
                style={{
                  border: "2px dotted #ccc",
                  height: "250px",
                  marginLeft: "15px",
                  width: "500px",
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center"
                }}
              >
                {this.props.image ? (
                  <img width="100%" src={image} alt="" />
                ) : (
                  <div>
                    <img width="100%" src={certificate} alt="" />
                    <Label
                      htmlFor="course-poster"
                      className="float-right"
                      sm={4}
                    >
                      <span className="btn btn-success">
                        <i className="fa fa-upload"></i> <br />
                        Upload Certificate
                      </span>
                    </Label>
                  </div>
                )}
              </div>
              <Input
                accept="image/*"
                id="course-poster"
                type="file"
                name="image"
                onChange={handleCertUpload}
                // invalid={!!errors.image}
                style={{ display: "none" }}
              />
            </FormGroup>
            <div style={{ display: "flex", justifyContent: "space-between" }}>
              <Button onClick={this.goBack} className="btn-style-two">
                <i className="fa fa-arrow-left"></i> Back
              </Button>
              <Button onClick={() => this.next(data)} className="btn-style-two">
                Submit <i className="fa fa-arrow-right"></i>
              </Button>
            </div>
          </CardBody>
        </Card>
      </React.Fragment>
    );
  }
}

export default Submit;
