import React from "react";
import { Button, Col, Container, Row, Card, CardBody } from "reactstrap";
import { Link } from "react-router-dom";
import successgif from "../../../img/success.gif";

const Success = () => {
  return (
    <Container>
      <Row>
        <Col>
          <Card>
            <CardBody>
              <div
                style={{
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  flexDirection: "column"
                }}
              >
                <React.Fragment>
                  <img
                    style={{ borderRadius: "5px", width: "30%" }}
                    src={successgif}
                    alt="success"
                  />
                  <p style={{ color: "green" }}>Course created successfully</p>
                </React.Fragment>
                <Link to="/courses">
                  <Button className="btn-style-two">Back to Courses</Button>
                </Link>
              </div>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </Container>
  );
};

export default Success;
