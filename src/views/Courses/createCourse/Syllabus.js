import React, { Component } from "react";
import {
  Button,
  FormGroup,
  Label,
  Card,
  CardBody,
  CardHeader
} from "reactstrap";
import CKEditor from "@ckeditor/ckeditor5-react";
import ClassicEditor from "@ckeditor/ckeditor5-build-classic";

class Syllabus extends Component {
  state = {
    errors: {}
  };

  continue = () => {
    const { step, nextStep } = this.props;

    nextStep(step);
  };

  goBack = () => {
    const { step } = this.props;
    this.props.prevStep(step);
  };
  render() {
    const { data, onChangeSyllabus } = this.props;

    return (
      <React.Fragment>
        <Card>
          <CardHeader>Course Syllabus</CardHeader>
          <CardBody>
            <div style={{ display: "flex", justifyContent: "space-between" }}>
              <Button onClick={this.goBack} className="btn-style-two">
                <i className="fa fa-arrow-left"></i> Back
              </Button>
              <Button onClick={this.continue} className="btn-style-two">
                Continue <i className="fa fa-arrow-right"></i>
              </Button>
            </div>

            <FormGroup>
              <Label>Enter Curriculum</Label>
              <CKEditor
                editor={ClassicEditor}
                data={data.syllabus}
                config={{
                  ckfinder: {
                    // Upload the images to the server using the CKFinder QuickUpload command.
                    uploadUrl:
                      "https://kenyachemicalsociety.org/app1/api/courses/media"
                  }
                }}
                onInit={editor => {
                  // You can store the "editor" and use when it is needed.
                  console.log("Editor is ready to use!", editor);
                }}
                onChange={(e, editor) => {
                  const data = editor.getData();
                  // onEditorCertChange(data, index);
                  onChangeSyllabus(data);
                }}
              />
            </FormGroup>
            <div style={{ display: "flex", justifyContent: "space-between" }}>
              <Button onClick={this.goBack} className="btn-style-two">
                <i className="fa fa-arrow-left"></i> Back
              </Button>
              <Button onClick={this.continue} className="btn-style-two">
                Continue <i className="fa fa-arrow-right"></i>
              </Button>
            </div>
          </CardBody>
        </Card>
      </React.Fragment>
    );
  }
}

export default Syllabus;
