import React, { Component } from "react";
import General from "./General";
import Description from "./Description";
import Success from "./Success";
import Submit from "./Submit";
import Content from "./Content";
import Syllabus from "./Syllabus";
import { connect } from "react-redux";
import {
  postCourse,
  fetchCourseById,
  updateCourse
} from "../../../store/actions/courses";

class CreateCourse extends Component {
  state = {
    data: {
      _id: this.props._id ? this.props._id : "",
      title: this.props.title ? this.props.title : "",
      author: this.props.author ? this.props.author : "",
      description: this.props.description ? this.props.description : "",
      image: this.props.image ? this.props.image : "",
      skills: this.props.skills ? this.props.skills : "",
      syllabus: this.props.syllabus ? this.props.syllabus : ""
    },
    content: this.props.content ? this.props.content : [],
    certificate: this.props.certificate ? this.props.certificate : "",
    step: 1,
    errors: {}
  };

  componentDidMount = () => {
    const { match } = this.props;
    if (match.params._id) {
      this.props.fetchCourseById(match.params._id);
    }
  };

  componentWillReceiveProps = nextProps => {
    console.log(nextProps);
    this.setState({
      data: {
        _id: nextProps.course._id,
        title: nextProps.course.title,
        image: nextProps.course.image,
        description: nextProps.course.description,
        author: nextProps.course.author,
        skills: nextProps.course.skills,
        syllabus: nextProps.course.syllabus
      },
      content: nextProps.course.content,
      certificate: nextProps.course.certificate
    });
  };

  nextStep = () => {
    const { step } = this.state;
    this.setState({ step: step + 1 });
  };
  prevStep = () => {
    const { step } = this.state;
    this.setState({ step: step - 1 });
  };

  onChange = e => {
    this.setState({
      data: { ...this.state.data, [e.target.name]: e.target.value }
    });
  };

  onChangeSkills = data => {
    this.setState({ data: { ...this.state.data, skills: data } });
  };

  onChangeDescription = data => {
    this.setState({ data: { ...this.state.data, description: data } });
  };
  onChangeSyllabus = data => {
    this.setState({ data: { ...this.state.data, syllabus: data } });
  };

  addSection = () => {
    this.setState({
      content: [...this.state.content, ""]
    });
  };

  deleteSection = index => {
    this.state.content.splice(index, 1);
    this.setState({
      content: this.state.content
    });
  };

  onEditorChange = (data, index) => {
    this.state.content[index] = data;
    this.setState({
      content: this.state.content
    });
  };
  onEditorCertChange = (data, index) => {
    this.state.certificate[index] = data;
    this.setState({
      certificate: this.state.certificate
    });
  };
  deleteCert = index => {
    this.state.certificate.splice(index, 1);
    this.setState({
      certificate: this.state.certificate
    });
  };

  handleFileUpload = e => {
    let image_as_base64 = URL.createObjectURL(e.target.files[0]);
    this.setState({ image: e.target.files[0], image_as_base64 });
  };

  handleCertUpload = e => {
    let cert_as_base64 = URL.createObjectURL(e.target.files[0]);
    this.setState({ cert: e.target.files[0], cert_as_base64 });
  };

  onSubmit = data => {
    let bodyFormData = new FormData();
    if (data._id) bodyFormData.set("_id", data._id);
    bodyFormData.set("description", data.description);
    bodyFormData.set("author", data.author);
    bodyFormData.set("title", data.title);
    bodyFormData.set("skills", data.skills);
    bodyFormData.set("syllabus", data.syllabus);
    const arr = this.state.content;
    for (let i = 0; i < arr.length; i++) {
      bodyFormData.append("content", arr[i]);
    }
    bodyFormData.append("upload", this.state.image);
    // bodyFormData.append("upload", this.state.cert);

    this.setState({ loading: true });
    if (data._id) {
      return this.props.updateCourse(bodyFormData);
    } else {
      return this.props.postCourse(bodyFormData);
    }
  };

  render() {
    const { step, data, errors } = this.state;
    console.log(this.state);
    switch (step) {
      case 1:
        return (
          <General
            nextStep={this.nextStep}
            step={step}
            onChangeSkills={this.onChangeSkills}
            data={data}
            errors={errors}
            onChange={this.onChange}
          />
        );
      case 2:
        return (
          <Description
            nextStep={this.nextStep}
            step={step}
            data={data}
            handleFileUpload={this.handleFileUpload}
            image={this.state.image_as_base64}
            errors={errors}
            prevStep={this.prevStep}
            onChangeDescription={this.onChangeDescription}
            onChange={this.onChange}
          />
        );

      case 3:
        return (
          <Syllabus
            nextStep={this.nextStep}
            step={step}
            data={data}
            onChange={this.onChange}
            errors={errors}
            onChangeSyllabus={this.onChangeSyllabus}
            prevStep={this.prevStep}
          />
        );
      case 4:
        return (
          <Content
            nextStep={this.nextStep}
            step={step}
            content={this.state.content}
            addSection={this.addSection}
            errors={errors}
            prevStep={this.prevStep}
            deleteSection={this.deleteSection}
            handleSectionChange={this.handleSectionChange}
            onEditorChange={this.onEditorChange}
            onChange={this.onChange}
          />
        );
      case 5:
        return (
          <Submit
            nextStep={this.nextStep}
            step={step}
            data={data}
            prevStep={this.prevStep}
            onSubmit={this.onSubmit}
            certificate={this.state.certificate}
            handleCertUpload={this.handleCertUpload}
            image={this.state.cert_as_base64}
          />
        );
      case 6:
        return <Success />;

      default:
        break;
    }
  }
}

function mapStateToProps(state, props) {
  const { match } = props;
  if (match.params._id) {
    return {
      course: state.course.find(item => item._id === match.params._id)
    };
  }

  return { course: null };
}

export default connect(mapStateToProps, {
  postCourse,
  fetchCourseById,
  updateCourse
})(CreateCourse);
