import React, { Component } from "react";
import Widget04 from "../Widgets/Widget04";
import { connect } from "react-redux";
import { fetchUsers } from "../../store/actions/users";
// import { Link } from "react-router-dom";
import Moment from "react-moment";
import {
  Card,
  CardBody,
  // CardFooter,
  CardHeader,
  // CardTitle,
  Col,
  Progress,
  CardGroup,
  Row,
  Table
} from "reactstrap";

class Dashboard extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.onRadioBtnClick = this.onRadioBtnClick.bind(this);

    this.state = {
      dropdownOpen: false,
      radioSelected: 2
    };
  }

  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    });
  }

  onRadioBtnClick(radioSelected) {
    this.setState({
      radioSelected: radioSelected
    });
  }

  componentDidMount() {
    this.props.fetchUsers();
  }

  render() {
    return (
      <div className="animated fadeIn">
        <Row>
          <Col>
            <CardGroup className="mb-4">
              <Widget04
                icon="icon-people"
                color="info"
                header={this.props.registeredUsers}
                value="25"
              >
                Registered Users
              </Widget04>
              <Widget04
                icon="icon-user-follow"
                color="success"
                header={this.props.activeUsers}
                value="25"
              >
                Active Members
              </Widget04>
              <Widget04
                icon="icon-people"
                color="warning"
                header={this.props.inactiveUsers}
                value="25"
              >
                Passive Members
              </Widget04>
              {/* <Widget04
                icon="icon-pie-chart"
                color="primary"
                header="28%"
                value="25"
              >
                Courses
              </Widget04> */}
              {/* <Widget04
                icon="icon-speedometer"
                color="danger"
                header="5:34:11"
                value="25"
              >
                Avg. Time
              </Widget04> */}
            </CardGroup>
          </Col>
        </Row>
        <Row>
          <Col>
            <Card>
              <CardHeader>Users Activities</CardHeader>
              <CardBody>
                <br />
                <Table
                  hover
                  responsive
                  className="table-outline mb-0 d-none d-sm-table"
                >
                  <thead className="thead-light">
                    <tr>
                      <th className="text-center">
                        <i className="icon-people" />
                      </th>
                      <th>User</th>
                      <th className="text-center">Phone</th>
                      <th>Usage</th>
                      <th className="text-center">Membership Fee</th>
                      <th>Activity</th>
                      <th>Status</th>
                    </tr>
                  </thead>
                  <tbody>
                    {this.props.users.map(user => (
                      <tr key={user._id}>
                        <td className="text-center">
                          <div className="avatar">
                            <img
                              src={user.avatar}
                              className="img-avatar"
                              alt={user.firstName}
                            />
                            <span className="avatar-status badge-success" />
                          </div>
                        </td>
                        <td>
                          <div>
                            {user.title} {user.firstName} {user.lastName}
                          </div>
                          <div className="small text-muted">
                            <span>New</span> | Registered:{" "}
                            <Moment fromNow>{user.createdAt}</Moment>
                          </div>
                        </td>
                        <td className="text-center">
                          <div>+254{user.phoneNumber}</div>
                        </td>
                        <td>
                          <div className="clearfix">
                            <div className="float-left">
                              <strong>1%</strong>
                            </div>
                            <div className="float-right">
                              <small className="text-muted">
                                <Moment fromNow ago>
                                  {user.created}
                                </Moment>
                              </small>
                            </div>
                          </div>
                          <Progress
                            className="progress-xs"
                            color="success"
                            value="1"
                          />
                        </td>
                        <td className="text-center">
                          {/* <i
                            className="fa fa-cc-mastercard"
                            style={{ fontSize: 24 + "px" }}
                          /> */}
                          <div>{user.paid === true ? "Paid" : "Not Paid"}</div>
                        </td>
                        <td>
                          <div className="small text-muted">Last login</div>
                          <strong>
                            <Moment fromNow>{user.createdAt}</Moment>
                          </strong>
                        </td>
                        <td>
                          <div>{user.status}</div>
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </Table>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    users: state.users,
    registeredUsers: state.users.length,
    inactiveUsers: state.users.filter(item => item.status === "Pending").length,
    activeUsers: state.users.filter(item => item.paid === true).length
    // payments: state.payments.map(item => item.OrgAccountBalance)
  };
}

export default connect(mapStateToProps, { fetchUsers })(Dashboard);
