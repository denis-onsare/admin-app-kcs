import React from "react";
import {
  Container,
  Row,
  Col,
  Form,
  FormGroup,
  Input,
  Label,
  Card,
  CardHeader,
  CardBody,
  Button
} from "reactstrap";

class CreateEvent extends React.Component {
  state = {};

  render() {
    return (
      <div>
        <Container>
          <Row>
            <Col>
              <Card>
                <CardHeader>New Event</CardHeader>
                <CardBody>
                  <Form>
                    <FormGroup>
                      <Label>Title</Label>
                      <Input type="text" placeholder="Event title" />
                    </FormGroup>
                    <FormGroup>
                      <Label>Event Date</Label>
                      <Input type="date" placeholder="Event Date" />
                    </FormGroup>
                    <FormGroup>
                      <Label>Event Description</Label>
                      <Input
                        type="textarea"
                        style={{ height: 200 }}
                        placeholder="Describe the event"
                      />
                    </FormGroup>
                    <Button>Submit</Button>
                  </Form>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default CreateEvent;
