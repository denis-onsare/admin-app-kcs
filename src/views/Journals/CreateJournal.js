import React from "react";

import {
  Container,
  Row,
  Col,
  Form,
  FormGroup,
  Input,
  Label,
  Card,
  CardHeader,
  CardBody,
  Button,
  FormFeedback
} from "reactstrap";

import { connect } from "react-redux";
import {
  postJournal,
  fetchJournalById,
  updateJournal
} from "../../store/actions/journal";
import LoadingState from "../../components/states/LoadingState";

class CreateJournal extends React.Component {
  state = {
    data: {
      _id: this.props.journal ? this.props.journal._id : null,
      attachment: this.props.journal ? this.props.journal.attachment : [],
      abstract: this.props.journal ? this.props.journal.abstract : "",
      // image: this.props.journal ? this.props.journal.image : "",
      title: this.props.journal ? this.props.journal.title : "",
      keywords: this.props.journal ? this.props.journal.keywords : "",
      author: this.props.journal ? this.props.journal.author : ""
    },
    errors: {},
    loading: false,
    progress: "Awaiting journal upload..."
  };
  componentDidMount = () => {
    const { match } = this.props;
    if (match.params._id) {
      this.props.fetchJournalById(match.params._id);
    }
  };

  componentWillReceiveProps = nextProps => {
    this.setState({
      data: {
        _id: nextProps.journal._id,
        title: nextProps.journal.title,
        // image: nextProps.journal.image,
        keywords: nextProps.journal.keywords,
        abstract: nextProps.journal.abstract,
        attachment: nextProps.journal.attachment,
        author: nextProps.journal.author
      }
    });
  };

  onSubmit = e => {
    e.preventDefault();
    const errors = this.validate(this.state.data);
    const data = this.state.data;
    this.setState({ errors });
    let bodyFormData = new FormData();
    if (data._id) bodyFormData.set("_id", data._id);
    bodyFormData.set("abstract", data.abstract);
    bodyFormData.set("author", data.author);
    bodyFormData.set("keywords", data.keywords);
    bodyFormData.set("title", data.title);
    bodyFormData.append("attachment", this.state.attachment);
    if (Object.keys(errors).length === 0) {
      this.setState({ loading: true });
      if (data._id) {
        this.props
          .updateJournal(bodyFormData)
          .then(() => this.props.history.push("/journals"))
          .catch(err =>
            this.setState({ errors: err.response.data, loading: false })
          );
      } else {
        this.props
          .postJournal(bodyFormData)
          .then(() => this.props.history.push("/journals"))
          .catch(err =>
            this.setState({ errors: err.response.data, loading: false })
          );
      }
    }
  };

  onChange = e => {
    this.setState({
      data: { ...this.state.data, [e.target.name]: e.target.value }
    });
  };

  handleFileUpload = e => {
    let image_as_base64 = URL.createObjectURL(e.target.files[0]);
    this.setState({ attachment: e.target.files[0], image_as_base64 });
  };

  validate = data => {
    const errors = {};

    if (!data.title) errors.title = "title is required";
    if (!data.author) errors.author = "author is required";
    // if (!data.attachment) errors.attachment = "attachment is required";
    if (!data.abstract) errors.abstract = "abstract is required";
    if (!data.keywords) errors.keywords = "keywords is required";
    return errors;
  };

  render() {
    const { data, errors, loading } = this.state;

    return (
      <div>
        <Container>
          <Row>
            <Col>
              <Card>
                <CardHeader>New Journal</CardHeader>
                <CardBody>
                  {loading && <LoadingState message="loading..." />}
                  <Form onSubmit={this.onSubmit}>
                    {errors.error ? (
                      <div className="alert alert-danger">{errors.error}</div>
                    ) : (
                      ""
                    )}
                    <FormGroup row>
                      <div
                        style={{
                          border: "2px dotted #ccc",
                          height: "250px",
                          width: "500px",
                          display: "flex",
                          justifyContent: "center",
                          alignItems: "center"
                        }}
                      >
                        {this.state.attachment ? (
                          <img width="100%" src="/assets/img/pdf.png" alt="" />
                        ) : (
                          <Label htmlFor="course-poster" sm={3}>
                            <span className="btn btn-success">
                              <i className="fa fa-upload"></i> Upload document
                            </span>
                          </Label>
                        )}
                      </div>
                      <Input
                        accept="image/*"
                        id="course-poster"
                        type="file"
                        name="image"
                        onChange={this.handleFileUpload}
                        // invalid={!!errors.image}
                        style={{ display: "none" }}
                      />
                    </FormGroup>
                    <FormGroup>
                      <Label>Title</Label>
                      <Input
                        type="text"
                        placeholder="Journal title"
                        name="title"
                        onChange={this.onChange}
                        value={data.title}
                        invalid={!!errors.title}
                      />
                      <FormFeedback>{errors.title}</FormFeedback>
                    </FormGroup>
                    <FormGroup>
                      <Label>Author</Label>
                      <Input
                        type="text"
                        placeholder="Authors Name"
                        name="author"
                        onChange={this.onChange}
                        value={data.author}
                        invalid={!!errors.author}
                      />
                      <FormFeedback>{errors.author}</FormFeedback>
                    </FormGroup>

                    <FormGroup>
                      <Label>Abstract</Label>
                      <Input
                        type="textarea"
                        style={{ height: 200 }}
                        placeholder="Abstract"
                        name="abstract"
                        onChange={this.onChange}
                        value={data.abstract}
                        invalid={!!errors.abstract}
                      />
                      <FormFeedback>{errors.abstract}</FormFeedback>
                    </FormGroup>
                    <FormGroup>
                      <Label>Keywords</Label>
                      <Input
                        type="text"
                        placeholder="journal keywords"
                        name="keywords"
                        onChange={this.onChange}
                        value={data.keywords}
                        invalid={!!errors.keywords}
                      />
                      <FormFeedback>{errors.keywords}</FormFeedback>
                    </FormGroup>
                    <Button>Submit</Button>
                  </Form>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

function mapStateToProps(state, props) {
  const { match } = props;
  if (match.params._id) {
    return {
      journal: state.journal.find(item => item._id === match.params._id)
    };
  }

  return { journal: null };
}

export default connect(mapStateToProps, {
  postJournal,
  fetchJournalById,
  updateJournal
})(CreateJournal);
