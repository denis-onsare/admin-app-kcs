import React from "react";
import {
  Container,
  Row,
  Col,
  Card,
  CardImg,
  CardBody,
  CardTitle,
  CardText,
  CardSubtitle,
  Button
} from "reactstrap";
import { Link } from "react-router-dom";

import { connect } from "react-redux";
import { fetchJournalById } from "../../store/actions/journal";
import Moment from "react-moment";

class Journal extends React.Component {
  state = {
    journal: {
      _id: this.props.journal ? this.props.journal._id : null,
      attachment: this.props.journal ? this.props.journal.attachment : [],
      abstract: this.props.journal ? this.props.journal.abstract : "",
      image: this.props.journal ? this.props.journal.image : "",
      title: this.props.journal ? this.props.journal.title : "",
      author: this.props.journal ? this.props.journal.author : ""
    }
  };
  componentDidMount = () => {
    const { match } = this.props;
    if (match.params._id) {
      this.props.fetchJournalById(match.params._id);
    }
  };

  componentWillReceiveProps = nextProps => {
    this.setState({
      journal: {
        _id: nextProps.journal._id,
        title: nextProps.journal.title,
        image: nextProps.journal.image,
        keywords: nextProps.journal.keywords,
        abstract: nextProps.journal.abstract,
        attachment: nextProps.journal.attachment,
        author: nextProps.journal.author
      }
    });
  };

  render() {
    const { journal } = this.state;

    return (
      <Container>
        <Row className="mt40 mb40">
          <Col sm={{ size: "8", offset: "2" }}>
            <Card>
              <CardImg
                top
                width="100%"
                height="350px"
                src={journal.image}
                alt={journal.title}
              />
              <CardBody>
                <CardTitle>{journal.title}</CardTitle>
                <CardTitle>{journal.author}</CardTitle>
                <CardTitle>{journal.keywords}</CardTitle>
                <CardSubtitle>
                  Date <Moment format="YYYY/MM/DD">{journal.createdAt}</Moment>
                </CardSubtitle>
                <CardText>{journal.abstract}</CardText>
                <div className="float-right">
                  <Link to={`/journals/edit/${journal._id}`}>
                    <Button color="success">Edit</Button>
                  </Link>
                </div>
              </CardBody>
            </Card>
          </Col>
        </Row>
        {/* <Row className="mt40 mb40">
          <Col sm={{ size: "8", offset: "2" }}>
            <Card>
              <CardHeader>Comments</CardHeader>
              <CardBody>no comment</CardBody>
            </Card>
          </Col>
        </Row> */}
      </Container>
    );
  }
}
function mapStateToProps(state, props) {
  const { match } = props;
  if (match.params._id) {
    return {
      journal: state.journal.find(item => item._id === match.params._id)
    };
  }

  return { journal: null };
}

export default connect(mapStateToProps, { fetchJournalById })(Journal);
