import React from "react";
import { Col, Button, Row } from "reactstrap";
import { Link } from "react-router-dom";

const JournalCard = ({ journal, deleteJournal }) => (
  <Row>
    <Col sm="4">
    <div>
      <img src="./assets/img/pdf.png" alt={journal.title} width="60%" />
    </div>
     
    </Col>
    <Col sm="8">
      <h6>Title: {journal.title}</h6>
      <h6>Author: {journal.author}</h6>
      <h6>Abstract</h6>
      {journal.abstract}
      <h6>Keywords: {journal.keywords}</h6>
      <div className="float-right">
        <Link to={`/journals/${journal._id}`}>
          <Button color="success" className="mr10">
            Edit
          </Button>
        </Link>
        <Button color="success" target="_blank" href={journal.attachment}>
          Download
        </Button>
        {" "}
        <Button color="danger" onClick={() => deleteJournal(journal._id)}>
          Delete
        </Button>
      </div>
    </Col>
  </Row>
);

export default JournalCard;
