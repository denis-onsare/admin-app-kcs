import React from "react";
import JournalCard from "./JournalCard";
import EmptyPage from "../../components/partials/EmptyPage";

class JournalList extends React.Component {
  state = {};

  render() {
    const journalList = (
      <div>
        <div>
          {this.props.journals.map(journal => (
            <JournalCard
              journal={journal}
              key={journal._id}
              deleteJournal={this.props.deleteJournal}
            />
          ))}
        </div>
      </div>
    );
    return (
      <div>
        {this.props.journals.length === 0 ? (
          <EmptyPage message="journals" />
        ) : (
          journalList
        )}
      </div>
    );
  }
}

export default JournalList;
