import React from "react";
import { connect } from "react-redux";
import { Container, Row, Card, CardHeader, CardBody, Col } from "reactstrap";

import { fetchJournals, deleteJournal } from "../../store/actions/journal";
import JournalList from "./JournalList";

class Journals extends React.Component {
  componentDidMount() {
    this.props.fetchJournals();
  }

  render() {
    return (
      <Container>
        <Row>
          <Col>
          
          <Card>
            <CardHeader>All Journals</CardHeader>
            <CardBody>
              <JournalList
                journals={this.props.journals}
                deleteJournal={this.props.deleteJournal}
              />
            </CardBody>
          </Card>
          </Col>
        </Row>
      </Container>
    );
  }
}

function mapStateToProps(state) {
  return {
    journals: state.journal
  };
}

export default connect(
  mapStateToProps,
  { fetchJournals, deleteJournal }
)(Journals);
