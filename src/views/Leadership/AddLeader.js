import React from "react";
import {
  Container,
  Row,
  Col,
  Form,
  FormGroup,
  Input,
  Label,
  Card,
  CardHeader,
  CardBody,
  Button,
  FormFeedback
} from "reactstrap";

import { connect } from "react-redux";
import { postLeadership } from "../../store/actions/leadership";
import LoadingState from "../../components/states/LoadingState";
import Dropzone from "react-dropzone";


class AddLeader extends React.Component {
  state = {
    data: {
      title: "",
      firstName: "",
      lastName: "",
      position: "",
      hierarchy: "",
      images: ""
    },
    errors: {},
    loading: false,
    progress: "Awaiting Profile Image..."
  };

  

  submit = data =>
    this.props
      .postLeadership(data)
      .then(() => this.props.history.push("/leadership/structure"));

  onSubmit = e => {
    e.preventDefault();
    const errors = this.validate(this.state.data);
    this.setState({ errors });
    console.log(this.state.data);
    if (Object.keys(errors).length === 0) {
      const data = this.state.data;
      this.setState({ loading: true });

      this.submit(data).catch(err => {
        if (err.response) {
          this.setState({ errors: err.response.data.errors, loading: false });
        } else if (err.request) {
          console.log(err.request);
        } else {
          console.log("Error", err.message);
        }
      });
    }
  };

  onChange = e => {
    this.setState({
      data: { ...this.state.data, [e.target.name]: e.target.value }
    });
  };

  validate = data => {
    const errors = {};
    if (!data.title) errors.title = "required field";
    if (!data.firstName) errors.firstName = "required field";
    if (!data.lastName) errors.lastName = "required field";
    if (!data.position) errors.position = "required field";
    if (!data.hierarchy) errors.hierarchy = "required field";

    return errors;
  };

  render() {
    const { data, errors, loading } = this.state;
    const profile = data.images;
    return (
      <div>
        <Container>
          <Row>
            <Col>
              <Card>
                <CardHeader>Add Leadership</CardHeader>
                <CardBody>
                  {loading && <LoadingState message="loading..." />}

                  <Form onSubmit={this.onSubmit}>
                    {errors.message && (
                      <div className="alert alert-danger">{errors.message}</div>
                    )}
                    <FormGroup>
                      <Label>Please click or drop image on the box below</Label>
                      <Dropzone onDrop={this.fileUpload} />
                      <div> {profile}</div>
                      <h2>{this.state.progress}</h2>
                    </FormGroup>
                    <FormGroup>
                      <Label>Title</Label>
                      <Input
                        disabled={data.images === ""}
                        type="select"
                        name="title"
                        id="title"
                        value={data.title}
                        onChange={this.onChange}
                        invalid={!!errors.title}
                      >
                        <option />
                        <option>Ms.</option>
                        <option>Mrs.</option>
                        <option>Mr.</option>
                        <option>Dr.</option>
                        <option>Prof.</option>
                      </Input>
                      <FormFeedback>{errors.title}</FormFeedback>
                    </FormGroup>
                    <FormGroup>
                      <Label>First Name</Label>
                      <Input
                        disabled={data.images === ""}
                        type="text"
                        name="firstName"
                        value={data.firstName}
                        onChange={this.onChange}
                        invalid={!!errors.firstName}
                      />
                      <FormFeedback>{errors.firstName}</FormFeedback>
                    </FormGroup>
                    <FormGroup>
                      <Label>Last Name</Label>
                      <Input
                        disabled={data.images === ""}
                        type="text"
                        name="lastName"
                        value={data.lastName}
                        onChange={this.onChange}
                        invalid={!!errors.lastName}
                      />
                      <FormFeedback>{errors.lastName}</FormFeedback>
                    </FormGroup>
                    <FormGroup>
                      <Label>Position</Label>
                      <Input
                        disabled={data.images === ""}
                        type="select"
                        name="position"
                        id="position"
                        value={data.position}
                        onChange={this.onChange}
                        invalid={!!errors.position}
                      >
                        <option />
                        <option>Chairman</option>
                        <option>Secretary</option>
                        <option>Vice Chair</option>
                        <option>Assistant Vice Secretary</option>
                        <option>Treasurer</option>
                        <option>Editor-In-Chief</option>
                        <option>Secretariat</option>
                        <option>Assistant Editor</option>
                      </Input>
                      <FormFeedback>{errors.position}</FormFeedback>
                    </FormGroup>

                    <FormGroup>
                      <Label>Hierarchy</Label>
                      <Input
                        disabled={data.images === ""}
                        type="select"
                        name="hierarchy"
                        id="hierarchy"
                        value={data.hierarchy}
                        onChange={this.onChange}
                        invalid={!!errors.hierarchy}
                      >
                        <option />
                        <option>Governing Council</option>
                        <option>Extended Governing Council</option>
                        <option>Nairobi Chapter</option>
                        <option>Coast Chapter</option>
                        <option>Western Chapter</option>
                        <option>Eastern Chapter</option>
                        <option>Mt.Kenya Chapter</option>
                        <option>North Rift Chapter</option>
                        <option>South Rift Chapter</option>
                      </Input>
                      <FormFeedback>{errors.hierarchy}</FormFeedback>
                    </FormGroup>

                    <Button>Submit</Button>
                  </Form>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default connect(null, { postLeadership })(AddLeader);
