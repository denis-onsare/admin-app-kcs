import React from "react";
import { connect } from "react-redux";

import LeadersList from "../LeadersList";
import { fetchLeadership } from "../../../store/actions/leadership";

class Leaders extends React.Component {
  componentDidMount() {
    this.props.fetchLeadership();
  }

  render() {
    return (
      <div>
        <LeadersList leaders={this.props.leaders} />
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    leaders: state.leadership.filter(
      item => item.hierarchy === "Governing Council"
    )
  };
}

export default connect(
  mapStateToProps,
  { fetchLeadership }
)(Leaders);
