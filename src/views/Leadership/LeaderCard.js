import React from "react";
import {
  Col,
  Card,
  CardBody,
  CardImg,
  CardTitle,
  CardSubtitle
} from "reactstrap";

const LeaderCard = ({ leader }) => (
  <Col sm="4">
    <Card className="mb40">
      <CardImg top width="100%" src={leader.image} alt={leader.firstName} />

      <CardBody>
        <CardTitle>
          {leader.title}
          {leader.firstName} {leader.lastName}
        </CardTitle>
        <CardSubtitle>Position: {leader.position}</CardSubtitle>
      </CardBody>
    </Card>
  </Col>
);

export default LeaderCard;
