import React from "react";
import { Container, Row } from "reactstrap";

import EmptyPage from "../../components/partials/EmptyPage";
import LeaderCard from "./LeaderCard";

const LeadersList = ({ leaders }) => {
  const membersList = (
    <Container>
      <Row className="pt40 pb40">
        {leaders.map(leader => <LeaderCard leader={leader} key={leader._id} />)}
      </Row>
    </Container>
  );

  return (
    <div>
      {leaders.length === 0 ? <EmptyPage message="leaders" /> : membersList}
    </div>
  );
};

export default LeadersList;
