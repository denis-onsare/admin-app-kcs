import React from "react";
import {
  Button,
  Card,
  CardBody,
  CardHeader,
  Col,
  Collapse,
  Row
} from "reactstrap";

import GoverningCouncil from "./Hierarchy/GoverningCouncil";
import ExtendedGoverningCouncil from "./Hierarchy/ExtendedGoverningCouncil";
import NairobiChapter from "./Hierarchy/NairobiChapter";
import WesternChapter from "./Hierarchy/WesternChapter";
import EasternChapter from "./Hierarchy/EasternChapter";
import SouthRiftChapter from "./Hierarchy/SouthRiftChapter";
import NorthRiftChapter from "./Hierarchy/NorthRiftChapter";
import MtKenyaChapter from "./Hierarchy/MtKenyaChapter";
import CoastChapter from "./Hierarchy/CoastChapter";

class Structure extends React.Component {
  constructor(props) {
    super(props);
    this.onEntering = this.onEntering.bind(this);
    this.onEntered = this.onEntered.bind(this);
    this.onExiting = this.onExiting.bind(this);
    this.onExited = this.onExited.bind(this);
    this.toggle = this.toggle.bind(this);
    this.toggleAccordion = this.toggleAccordion.bind(this);
    this.toggleCustom = this.toggleCustom.bind(this);
    this.toggleFade = this.toggleFade.bind(this);
    this.state = {
      collapse: false,
      accordion: [true, false, false, false, false, false, false, false, false],
      custom: [true, false],
      status: "Closed",
      fadeIn: true,
      timeout: 300
    };
  }

  onEntering() {
    this.setState({ status: "Opening..." });
  }

  onEntered() {
    this.setState({ status: "Opened" });
  }

  onExiting() {
    this.setState({ status: "Closing..." });
  }

  onExited() {
    this.setState({ status: "Closed" });
  }

  toggle() {
    this.setState({ collapse: !this.state.collapse });
  }

  toggleAccordion(tab) {
    const prevState = this.state.accordion;
    const state = prevState.map((x, index) => (tab === index ? !x : false));

    this.setState({
      accordion: state
    });
  }

  toggleCustom(tab) {
    const prevState = this.state.custom;
    const state = prevState.map((x, index) => (tab === index ? !x : false));

    this.setState({
      custom: state
    });
  }

  toggleFade() {
    this.setState({ fadeIn: !this.state.fadeIn });
  }

  render() {
    return (
      <div className="animated fadeIn">
        <Row className="pt40 pb40">
          <Col >
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify" /> KCS Leadership Structure{" "}
              </CardHeader>
              <CardBody>
                <div id="accordion">
                  <Card>
                    <Col sm={{ size: "4", offset: "4" }}>
                      <Card>
                        <CardHeader id="headingOne">
                          <Button
                            block
                            color="link"
                            className="text-left m-0 p-0"
                            onClick={() => this.toggleAccordion(0)}
                            aria-expanded={this.state.accordion[0]}
                            aria-controls="collapseOne"
                          >
                            <h5 className="m-0 p-0">Governing Council</h5>
                          </Button>
                        </CardHeader>
                      </Card>
                    </Col>
                    <Collapse
                      isOpen={this.state.accordion[0]}
                      data-parent="#accordion"
                      id="collapseOne"
                      aria-labelledby="headingOne"
                    >
                      <CardBody className="gc">
                        <Row>
                          <GoverningCouncil />
                        </Row>
                        <div className="mt20" />
                      </CardBody>
                    </Collapse>
                  </Card>
                  <div className="mt40" />
                  <Card>
                    <Col sm={{ size: "4", offset: "4" }}>
                      <Card>
                        <CardHeader id="headingTwo">
                          <Button
                            block
                            color="link"
                            className="text-left m-0 p-0"
                            onClick={() => this.toggleAccordion(1)}
                            aria-expanded={this.state.accordion[1]}
                            aria-controls="collapseTwo"
                          >
                            <h6 className="m-0 p-0">
                              Extended Governing Council
                            </h6>
                          </Button>
                        </CardHeader>
                      </Card>
                    </Col>

                    <Collapse
                      isOpen={this.state.accordion[1]}
                      data-parent="#accordion"
                      id="collapseTwo"
                    >
                      <CardBody>
                        <Row>
                          <ExtendedGoverningCouncil />
                        </Row>
                      </CardBody>
                    </Collapse>
                  </Card>
                  <div className="mt40" />
                  <Card>
                    <Row>
                      <Col sm="3">
                        <Card>
                          <CardHeader id="headingThree">
                            <Button
                              block
                              color="link"
                              className="text-left m-0 p-0"
                              onClick={() => this.toggleAccordion(2)}
                              aria-expanded={this.state.accordion[2]}
                              aria-controls="collapseThree"
                            >
                              <h6 className="m-0 p-0">Nairobi Chapter</h6>
                            </Button>
                          </CardHeader>
                        </Card>
                      </Col>
                      <Col sm="3">
                        <Card>
                          <CardHeader id="headingFour">
                            <Button
                              block
                              color="link"
                              className="text-left m-0 p-0"
                              onClick={() => this.toggleAccordion(3)}
                              aria-expanded={this.state.accordion[3]}
                              aria-controls="collapseFour"
                            >
                              <h6 className="m-0 p-0">Coast Chapter</h6>
                            </Button>
                          </CardHeader>
                        </Card>
                      </Col>
                      <Col sm="3">
                        <Card>
                          <CardHeader id="headingFive">
                            <Button
                              block
                              color="link"
                              className="text-left m-0 p-0"
                              onClick={() => this.toggleAccordion(4)}
                              aria-expanded={this.state.accordion[4]}
                              aria-controls="collapseFive"
                            >
                              <h6 className="m-0 p-0">Western Chapter</h6>
                            </Button>
                          </CardHeader>
                        </Card>
                      </Col>
                      <Col sm="3">
                        <Card>
                          <CardHeader id="headingSix">
                            <Button
                              block
                              color="link"
                              className="text-left m-0 p-0"
                              onClick={() => this.toggleAccordion(5)}
                              aria-expanded={this.state.accordion[5]}
                              aria-controls="collapseSix"
                            >
                              <h6 className="m-0 p-0">Eastern Chapter</h6>
                            </Button>
                          </CardHeader>
                        </Card>
                      </Col>
                    </Row>
                    <div className="pt40" />
                    <Row>
                      <Col sm="3">
                        <Card>
                          <CardHeader id="headingSeven">
                            <Button
                              block
                              color="link"
                              className="text-left m-0 p-0"
                              onClick={() => this.toggleAccordion(6)}
                              aria-expanded={this.state.accordion[6]}
                              aria-controls="collapseSeven"
                            >
                              <h6 className="m-0 p-0">Mt.Kenya Chapter</h6>
                            </Button>
                          </CardHeader>
                        </Card>
                      </Col>
                      <Col sm="3">
                        <Card>
                          <CardHeader id="headingEight">
                            <Button
                              block
                              color="link"
                              className="text-left m-0 p-0"
                              onClick={() => this.toggleAccordion(7)}
                              aria-expanded={this.state.accordion[7]}
                              aria-controls="collapseEight"
                            >
                              <h6 className="m-0 p-0">North-Rift Chapter</h6>
                            </Button>
                          </CardHeader>
                        </Card>
                      </Col>
                      <Col sm="3">
                        <Card>
                          <CardHeader id="headingNine">
                            <Button
                              block
                              color="link"
                              className="text-left m-0 p-0"
                              onClick={() => this.toggleAccordion(8)}
                              aria-expanded={this.state.accordion[8]}
                              aria-controls="collapseNine"
                            >
                              <h6 className="m-0 p-0">South-Rift Chapter</h6>
                            </Button>
                          </CardHeader>
                        </Card>
                      </Col>
                    </Row>
                    <Collapse
                      isOpen={this.state.accordion[2]}
                      data-parent="#accordion"
                      id="collapseThree"
                    >
                      <CardBody className="gc">
                        <Row>
                          <NairobiChapter />
                        </Row>
                      </CardBody>
                    </Collapse>
                    <Collapse
                      isOpen={this.state.accordion[3]}
                      data-parent="#accordion"
                      id="collapseFour"
                    >
                      <CardBody>
                        <Row>
                          <CoastChapter />
                        </Row>
                      </CardBody>
                    </Collapse>
                    <Collapse
                      isOpen={this.state.accordion[4]}
                      data-parent="#accordion"
                      id="collapseFive"
                    >
                      <CardBody>
                        <Row>
                          <WesternChapter />
                        </Row>
                      </CardBody>
                    </Collapse>
                    <Collapse
                      isOpen={this.state.accordion[5]}
                      data-parent="#accordion"
                      id="collapseSix"
                    >
                      <CardBody>
                        <Row>
                          <EasternChapter />
                        </Row>
                      </CardBody>
                    </Collapse>
                    <Collapse
                      isOpen={this.state.accordion[6]}
                      data-parent="#accordion"
                      id="collapseSeven"
                    >
                      <CardBody>
                        <Row>
                          <MtKenyaChapter />
                        </Row>
                      </CardBody>
                    </Collapse>
                    <Collapse
                      isOpen={this.state.accordion[7]}
                      data-parent="#accordion"
                      id="collapseEight"
                    >
                      <CardBody>
                        <Row>
                          <NorthRiftChapter />
                        </Row>
                      </CardBody>
                    </Collapse>
                    <Collapse
                      isOpen={this.state.accordion[8]}
                      data-parent="#accordion"
                      id="collapseNine"
                    >
                      <CardBody>
                        <Row>
                          <SouthRiftChapter />
                        </Row>
                      </CardBody>
                    </Collapse>
                  </Card>
                </div>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Structure;
