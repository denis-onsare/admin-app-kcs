import React from "react";
import {
  Container,
  Row,
  Col,
  Card,
  CardImg,
  CardBody,
  CardTitle,
  CardHeader,
  CardText,
  CardSubtitle,
  Button
} from "reactstrap";
import { Link } from "react-router-dom";

import { connect } from "react-redux";
import { fetchMeetingById } from "../../store/actions/meeting";
import Moment from "react-moment";

class Meeting extends React.Component {
  state = {
    meeting: {
      _id: this.props.meeting ? this.props.meeting._id : null,
      image: this.props.meeting ? this.props.meeting.images[0] : "",
      description: this.props.meeting ? this.props.meeting.description : "",
      title: this.props.meeting ? this.props.meeting.title : "",
      venue: this.props.meeting ? this.props.meeting.content : "",
      date: this.props.meeting ? this.props.meeting.date : ""
    }
  };
  componentDidMount = () => {
    const { match } = this.props;
    if (match.params._id) {
      this.props.fetchMeetingById(match.params._id);
    }
  };

 

  componentWillReceiveProps = nextProps => {
    this.setState({
      meeting: {
        _id: nextProps.meeting._id,
        title: nextProps.meeting.title,
        venue: nextProps.meeting.venue,
        description: nextProps.meeting.description,
        image: nextProps.meeting.images,
        date: nextProps.meeting.date
      }
    });
  };

  render() {
    const { meeting } = this.state;

    return (
      <Container>
        <Row className="mt40 mb40">
          <Col sm={{ size: "8", offset: "2" }}>
            <Card>
              <CardImg
                top
                width="100%"
                height="350px"
                src={meeting.image}
                alt={meeting.title}
              />
              <CardBody>
                <CardTitle>{meeting.title}</CardTitle>
                <CardSubtitle>
                  Date <Moment format="YYYY/MM/DD">{meeting.date}</Moment>
                </CardSubtitle>
                <CardText>{meeting.description}</CardText>
                <div className="float-right">
                  <Link to={`/meetings/edit/${meeting._id}`}>
                    <Button color="success">Edit</Button>
                  </Link>
                </div>
              </CardBody>
            </Card>
          </Col>
        </Row>
        <Row className="mt40 mb40">
          <Col sm={{ size: "8", offset: "2" }}>
            <Card>
              <CardHeader>Comments</CardHeader>
              <CardBody>no comment</CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
    );
  }
}
function mapStateToProps(state, props) {
  const { match } = props;
  if (match.params._id) {
    return {
      meeting: state.meeting.find(item => item._id === match.params._id)
    };
  }

  return { meeting: null };
}

export default connect(
  mapStateToProps,
  { fetchMeetingById }
)(Meeting);
