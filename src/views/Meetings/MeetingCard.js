import React from "react";
import { Row, Col, Button } from "reactstrap";
import Moment from "react-moment";
import { Link } from "react-router-dom";

const MeetingCard = ({ meeting, deleteMeeting }) => (
  <Row className="mb20" key={meeting._id}>
    <Col sm="5">
      <img src={meeting.images[0]} width="100%" alt={meeting.title} />
    </Col>
    <Col sm="7">
      <h6>Title: {meeting.title}</h6>
      <h6>Description</h6>
      <p>{meeting.description}</p>
      <h6>
        Date: <Moment format="YYYY/MM/DD">{meeting.date}</Moment>
      </h6>
      <h6>Venue: {meeting.venue}</h6>
      <div className="float-right">
        <Link to={`/meetings/${meeting._id}`}>
          <Button color="success" className="mr10">
            View Details
          </Button>
        </Link>
        &nbsp;
        <Button color="danger" onClick={() => deleteMeeting(meeting._id)}>
          Delete
        </Button>
      </div>
    </Col>
  </Row>
);

export default MeetingCard;
