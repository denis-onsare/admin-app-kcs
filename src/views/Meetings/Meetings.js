import React from "react";
import { connect } from "react-redux";
import { Container, Row, Card, CardHeader, CardBody, Col } from "reactstrap";

import { fetchMeetings, deleteMeeting } from "../../store/actions/meeting";
import MeetingsList from "./MeetingsList";

class Meetings extends React.Component {
  componentDidMount() {
    this.props.fetchMeetings();
  }

  render() {
    return (
      <Container>
        <Row>
          <Col>
          <Card>
            <CardHeader>All Meetings</CardHeader>
            <CardBody>
              <MeetingsList
                meetings={this.props.meetings}
                deleteMeeting={this.props.deleteMeeting}
              />
            </CardBody>
          </Card>
          </Col>
        </Row>
      </Container>
    );
  }
}

function mapStateToProps(state) {
  return {
    meetings: state.meeting
  };
}

export default connect(
  mapStateToProps,
  { fetchMeetings, deleteMeeting }
)(Meetings);
