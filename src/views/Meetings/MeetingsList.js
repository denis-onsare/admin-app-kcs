import React from "react";
import MeetingCard from "./MeetingCard";
import EmptyPage from "../../components/partials/EmptyPage";

class MeetingsList extends React.Component {
  state = {};

  render() {
    const meetingsList = (
      <div>
        <div>
          {this.props.meetings.map(meeting => (
            <MeetingCard
              meeting={meeting}
              key={meeting._id}
              deleteMeeting={this.props.deleteMeeting}
            />
          ))}
        </div>
      </div>
    );
    return (
      <div>
        {this.props.meetings.length === 0 ? (
          <EmptyPage message="meetings" />
        ) : (
          meetingsList
        )}
      </div>
    );
  }
}

export default MeetingsList;
