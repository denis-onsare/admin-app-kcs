import React from "react";
import {
  CustomInput,
  Button,
  Form,
  FormGroup,
  Label,
  Input,
  Col,
  Row,
  FormFeedback,
  Card,
  CardBody,
  CardHeader,
  Container
} from "reactstrap";
import { isEmail } from "validator";
import LoadingState from "../../components/states/LoadingState";
import { connect } from "react-redux";
import { register } from "../../store/actions/user";

class RegisterForm extends React.Component {
  state = {
    data: {
      title: "",
      gender: "",
      firstName: "",
      middleName: "",
      lastName: "",
      email: "",

      password: "",
      confirmPassword: "",
      areaCode: "",
      phoneNumber: "",
      nationalID: "",

      streetAddressLine1: "",
    
      city: "",
      state: "",
      chapter: "",
      membership: "",
    
    },
    errors: {},
    loading: false
  };

  onChange = e => {
    this.setState({
      data: { ...this.state.data, [e.target.name]: e.target.value }
    });
  };

  submit = data =>
    this.props.register(data).then(() => this.props.history.push("/members"));

  onSubmit = e => {
    e.preventDefault();
    const errors = this.validate(this.state.data);
    this.setState({ errors });
    if (Object.keys(errors).length === 0) {
      const data = this.state.data;
      this.setState({ loading: true });

      this.submit(data).catch(err => {
        if (err.response) {
          this.setState({ errors: err.response.data, loading: false });
        } else if (err.request) {
          console.log(err.request);
        } else {
          console.log("Error", err.message);
        }
      });
    }
  };
  validate = data => {
    const errors = {};
    if (!data.title) errors.title = "Title should not be empty";
    if (!data.gender) errors.gender = "Gender should not be empty";
    if (!data.firstName) errors.firstName = "First name should not be empty";
    if (!data.lastName) errors.lastName = "Last name should not be empty";
    if (!data.email)
      errors.email = "Primary email should not be empty";
    if (!isEmail(data.email))
      errors.email = "Primary Email must be a valid email";

    if (!data.password) errors.password = "Password should not be empty";
    if (data.confirmPassword !== data.password)
      errors.confirmPassword = "passwords do not match";
    if (!data.confirmPassword)
      errors.confirmPassword = "Confirm Password should not be empty";

    if (!data.areaCode) errors.areaCode = "Area code should not be empty";
    if (!data.phoneNumber)
      errors.phoneNumber = "Phone number should not be empty";
    if (!data.nationalID) errors.nationalID = "National Id should not be empty";

    if (!data.streetAddressLine1)
      errors.streetAddressLine1 = "Address Line 1 should not be empty";
    if (!data.chapter) errors.chapter = "Chapter should not be empty";
    if (!data.membership) errors.membership = "Membership should not be empty";

    return errors;
  };

  render() {
    const { data, errors, loading } = this.state;
    const message = "Loading...";
    return (
      <Container>
        <Row>
          <Col>
            <Card>
              <CardHeader>Create New Member</CardHeader>
              <CardBody>
                {loading && <LoadingState message={message} />}
                <Form onSubmit={this.onSubmit}>
                  {errors.error ? (
                      <div className="alert alert-danger">{errors.error}</div>
                    ) : (
                      ""
                    )}
                  <FormGroup>
                    <Label for="title">
                      Title <span className="required">*</span>
                    </Label>
                    <Input
                      type="select"
                      name="title"
                      id="title"
                      value={data.title}
                      onChange={this.onChange}
                      invalid={!!errors.title}
                    >
                      <option />
                      <option>Ms.</option>
                      <option>Mrs.</option>
                      <option>Mr.</option>
                      <option>Dr.</option>
                      <option>Prof.</option>
                    </Input>
                    <FormFeedback>{errors.title}</FormFeedback>
                    <span>Ms. Mrs. Mr. Dr. Prof.</span>
                  </FormGroup>
                  <FormGroup>
                    <Label for="gender">
                      Gender <span className="required">*</span>
                    </Label>
                    <div>
                      <CustomInput
                        type="radio"
                        id="male"
                        label="Male"
                        name="gender"
                        inline
                        value="male"
                        onChange={this.onChange}
                        invalid={!!errors.gender}
                      />

                      <CustomInput
                        type="radio"
                        id="female"
                        label="Female"
                        name="gender"
                        inline
                        value="female"
                        onChange={this.onChange}
                        invalid={!!errors.gender}
                      />
                      <p className="error">
                        {data.gender === "" && errors.gender}
                      </p>
                    </div>
                  </FormGroup>

                  <Row>
                    <Col sm="4">
                      <FormGroup>
                        <Label>
                          First Name <span className="required">*</span>
                        </Label>
                        <Input
                          type="text"
                          name="firstName"
                          value={data.firstName}
                          onChange={this.onChange}
                          invalid={!!errors.firstName}
                        />
                        <FormFeedback>{errors.firstName}</FormFeedback>
                      </FormGroup>
                    </Col>
                    <Col sm="4">
                      <FormGroup>
                        <Label>Middle Name</Label>
                        <Input
                          type="text"
                          name="middleName"
                          value={data.middleName}
                          onChange={this.onChange}
                        />
                      </FormGroup>
                    </Col>
                    <Col sm="4">
                      <FormGroup>
                        <Label>
                          Last Name <span className="required">*</span>
                        </Label>
                        <Input
                          type="text"
                          name="lastName"
                          value={data.lastName}
                          onChange={this.onChange}
                          invalid={!!errors.lastName}
                        />
                        <FormFeedback>{errors.lastName}</FormFeedback>
                      </FormGroup>
                    </Col>
                  </Row>

                  <FormGroup>
                    <Label>
                      Primary Email <span className="required">*</span>
                    </Label>
                    <Input
                      type="email"
                      placeholder="ex: myname@example.com"
                      name="email"
                      value={data.email}
                      onChange={this.onChange}
                      invalid={!!errors.email || !!errors.message}
                    />
                    <FormFeedback>{errors.email}</FormFeedback>
                    <div className="error">
                      {errors.message ? errors.message.email : null}
                    </div>
                    <span>example@example.com</span>
                  </FormGroup>

                  <FormGroup>
                    <Label>
                      Choose Password <span className="required">*</span>
                    </Label>
                    <Input
                      type="password"
                      name="password"
                      value={data.password}
                      onChange={this.onChange}
                      invalid={!!errors.password}
                    />
                    <FormFeedback>{errors.password}</FormFeedback>
                  </FormGroup>
                  <FormGroup>
                    <Label>
                      Confirm Your Chosen Password{" "}
                      <span className="required">*</span>
                    </Label>
                    <Input
                      type="password"
                      name="confirmPassword"
                      value={data.confirmPassword}
                      onChange={this.onChange}
                      invalid={!!errors.confirmPassword}
                    />
                    <FormFeedback>{errors.confirmPassword}</FormFeedback>
                  </FormGroup>

                  <FormGroup>
                    <Label>
                      Phone Number <span className="required">*</span>
                    </Label>
                    <Row>
                      <Col sm="3">
                        <Input
                          type="text"
                          placeholder="ex: +254"
                          name="areaCode"
                          value={data.areaCode}
                          onChange={this.onChange}
                          invalid={!!errors.areaCode}
                        />
                        <FormFeedback>{errors.areaCode}</FormFeedback>
                        <span>Area Code</span>
                      </Col>
                      <Col sm="4">
                        <Input
                          type="text"
                          placeholder="ex: 0712345678 "
                          name="phoneNumber"
                          value={data.phoneNumber}
                          onChange={this.onChange}
                          invalid={!!errors.phoneNumber}
                        />
                        <FormFeedback>{errors.phoneNumber}</FormFeedback>
                        <span>Phone number</span>
                      </Col>
                    </Row>
                  </FormGroup>
                  <FormGroup>
                    <Label>
                      National ID Number <span className="required">*</span>
                    </Label>
                    <Input
                      type="text"
                      placeholder="ex: 123"
                      name="nationalID"
                      value={data.nationalID}
                      onChange={this.onChange}
                      invalid={!!errors.nationalID}
                    />
                    <FormFeedback>{errors.nationalID}</FormFeedback>
                    <span>1234567890</span>
                  </FormGroup>

                  <FormGroup>
                    <Label>
                      Address: <span className="required">*</span>
                    </Label>
                    <Input
                      type="text"
                      name="streetAddressLine1"
                      value={data.streetAddressLine1}
                      onChange={this.onChange}
                      invalid={!!errors.streetAddressLine1}
                    />
                    <FormFeedback>{errors.streetAddressLine1}</FormFeedback>
                    <span>Street Address Line 1</span>
                   
                    <Row>
                      <Col sm="4">
                        <Input
                          type="text"
                          name="city"
                          value={data.city}
                          onChange={this.onChange}
                        />
                        <span>City</span>
                      </Col>
                      <Col sm="4">
                        <Input
                          type="text"
                          name="state"
                          value={data.state}
                          onChange={this.onChange}
                        />
                        <span>State / Province</span>
                      </Col>
                    </Row>
                  </FormGroup>
                  <FormGroup>
                    <Label>
                      Chapter <span className="required">*</span>
                    </Label>
                    <Input
                      type="select"
                      name="chapter"
                      id="chapter"
                      value={data.chapter}
                      onChange={this.onChange}
                      invalid={!!errors.chapter}
                    >
                      <option />

                      <option>Mount Kenya</option>
                      <option>Coast</option>
                      <option>Nairobi</option>
                      <option>Eastern</option>
                      <option>North Rift</option>
                      <option>South Rift</option>
                      <option>Western</option>
                      <option>Life</option>
                      <option>Student</option>
                    </Input>
                    <FormFeedback>{errors.chapter}</FormFeedback>
                    <span>Nairobi, Coast, Western</span>
                  </FormGroup>
                  <FormGroup>
                    <Label>
                      Membership <span className="required">*</span>
                    </Label>
                    <Input
                      type="select"
                      name="membership"
                      id="membership"
                      value={data.membership}
                      onChange={this.onChange}
                      invalid={!!errors.membership}
                    >
                      <option />
                      <option>Life Member</option>
                      <option>Ordinary Member</option>
                      <option>Associate Member</option>
                      <option>Student Member</option>
                    </Input>
                    <FormFeedback>{errors.membership}</FormFeedback>
                    <span>Life, Ordinary, Associate, Student</span>
                  </FormGroup>
                  <FormGroup>
                    <Input
                      type="hidden"
                      name="role"
                      id="role"
                      value={data.role}
                      onChange={this.onChange}
                    />
                  </FormGroup>
                  <Button>Submit</Button>
                </Form>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default connect(
  null,
  { register }
)(RegisterForm);
