import React from "react";
import {
  Col,
  Card,
  CardBody,
  CardImg,
  CardTitle,
  CardSubtitle
} from "reactstrap";

const MemberCard = ({ member }) => (
  <Col sm="3">
    <Card className="mb40">
      <CardImg top width="100%" src={member.avatar} alt={member.firstName} />

      <CardBody>
        <CardTitle>
          {member.title}
          {member.firstName} {member.lastName}
        </CardTitle>
        <CardSubtitle>Membership: {member.membership}</CardSubtitle>
        <br />
        <CardSubtitle>Chapter: {member.chapter}</CardSubtitle>
      </CardBody>
    </Card>
  </Col>
);

export default MemberCard;
