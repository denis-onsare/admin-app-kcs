import React from "react";
import { connect } from "react-redux";
import { Container, Row, Card, CardHeader, CardBody, Col } from "reactstrap";

import MembersList from "./MembersList";
import { fetchUsers } from "../../store/actions/users";

class Members extends React.Component {
  componentDidMount() {
    this.props.fetchUsers();
  }

  render() {
    return (
      <Container>
        <Row>
          <Col>
          <Card>
            <CardHeader>All Members</CardHeader>
            <CardBody>
              <MembersList members={this.props.members} />
            </CardBody>
          </Card>
          </Col>
        </Row>
      </Container>
    );
  }
}

function mapStateToProps(state) {
  return {
    members: state.users
  };
}

export default connect(
  mapStateToProps,
  { fetchUsers }
)(Members);
