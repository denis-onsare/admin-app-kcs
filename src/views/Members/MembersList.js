import React from "react";
import { Container, Row } from "reactstrap";

import EmptyPage from "../../components/partials/EmptyPage";
import MemberCard from "./MemberCard";

const MembersList = ({ members }) => {
  const membersList = (
    <Container>
      <Row className="pt40 pb40">
        {members.map(member => <MemberCard member={member} key={member._id} />)}
      </Row>
    </Container>
  );

  return (
    <div>
      {members.length === 0 ? <EmptyPage message="members" /> : membersList}
    </div>
  );
};

export default MembersList;
