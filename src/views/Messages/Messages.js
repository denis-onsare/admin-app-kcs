import React from "react";
import { Container, Col, Row, Card, CardBody, CardHeader } from "reactstrap";
import { connect } from "react-redux";
import { fetchMessages } from "../../store/actions/messages";
import Moment from "react-moment";

class Messages extends React.Component {
  state = {};

  componentDidMount = () => {
    this.props.fetchMessages();
  };
  render() {
    return (
      <div>
        <Container>
          <Row className="pt40 pb40">
            <Col>
              <Card>
                <CardHeader> Messages</CardHeader>
                <CardBody>
                  {this.props.messages.map(message => (
                    <div className="message" key={message._id}>
                      MessageID: {message._id}
                      <br />
                      Message: {message.text} <br />
                      <small>
                        Sent: <Moment fromNow>{message.createdAt}</Moment>
                      </small>{" "}
                      <small>BY:{message.name}</small>
                    </div>
                  ))}
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    messages: state.messages
  };
}

export default connect(mapStateToProps, { fetchMessages })(Messages);
