import React from "react";
import { Container, Col, Row, Card, CardBody, CardHeader } from "reactstrap";

const Notifications = () => (
  <Container>
    <Row className="pt40 pb40">
      <Col sm="6">
        <Card>
          <CardHeader> Notifications</CardHeader>
          <CardBody>no notifications found</CardBody>
        </Card>
      </Col>
    </Row>
  </Container>
);

export default Notifications;
