import React, { Component } from "react";
import {
  Button,
  Card,
  CardBody,
  CardGroup,
  Col,
  Container,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row,
  Form,
  FormFeedback
} from "reactstrap";
import { isEmail } from "validator";

import logo from "../../../assets/img/brand/logo.JPG";

class Login extends Component {
  state = {
    data: {
      email: "",
      password: ""
    },
    errors: {},
    serverErrors: {},
    loading: false
  };

  onChange = e => {
    this.setState({
      data: { ...this.state.data, [e.target.name]: e.target.value }
    });
  };

  onSubmit = e => {
    e.preventDefault();
    const { data } = this.state;
    const errors = this.validate(data);
    this.setState({ errors });
    if (Object.keys(errors).length === 0) {
      this.setState({ loading: true });
      this.props
        .submit(data)
        .catch(err =>
          this.setState({ serverErrors: err.response, loading: false })
        );
    }
  };

  validate = data => {
    const errors = {};

    if (!data.email) errors.email = "Email should not be empty";
    if (!isEmail(data.email)) errors.email = "Email must be a valid email";
    if (!data.password) errors.password = "Password should not be empty";

    return errors;
  };
  render() {
    const { errors, data, serverErrors } = this.state;
    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="8">
              <CardGroup>
                <Card className="p-4">
                  <CardBody>
                    <h1>Admin Login</h1>
                    <p className="text-muted">Sign In to your account</p>
                    {Object.keys(errors).length > 0 && (
                      <div className="alert alert-danger">{errors.data}</div>
                    )}
                    {Object.keys(serverErrors).length > 0 && (
                      <div className="alert alert-danger">
                        {serverErrors.data.error}
                      </div>
                    )}
                    <Form onSubmit={this.onSubmit}>
                      <InputGroup className="mb-3">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-user" />
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input
                          type="email"
                          onChange={this.onChange}
                          name="email"
                          value={data.email}
                          placeholder="Email"
                          invalid={!!errors.email}
                        />
                        <FormFeedback>{errors.email}</FormFeedback>
                      </InputGroup>
                      <InputGroup className="mb-4">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock" />
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input
                          type="password"
                          name="password"
                          onChange={this.onChange}
                          value={data.password}
                          invalid={!!errors.password}
                          placeholder="Password"
                        />
                        <FormFeedback>{errors.password}</FormFeedback>
                      </InputGroup>

                      <Row>
                        <Col xs="6">
                          <Button color="primary" className="px-4">
                            Login
                          </Button>
                        </Col>
                        {/* <Col xs="6" className="text-right">
                          <Button color="link" className="px-0">
                            Forgot password?
                          </Button>
                        </Col> */}
                      </Row>
                    </Form>
                  </CardBody>
                </Card>
                <Card
                  className="text-white bg-primary py-5 d-md-down-none"
                  style={{ width: 44 + "%" }}
                >
                  <CardBody className="text-center">
                    <div>
                      <img
                        style={{
                          border: "5px solid orange",
                          borderRadius: "15px"
                        }}
                        src={logo}
                        alt="logo"
                      />
                      <br />
                      <Button
                        color="primary"
                        target="_blank"
                        href="https://kenyachemicalsociety.org"
                        className="mt-3"
                        active
                      >
                        Visit Main Site
                      </Button>
                    </div>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default Login;
