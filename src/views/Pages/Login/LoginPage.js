import React from "react";
import { connect } from "react-redux";

import LoginForm from "./Login";
import { login } from "../../../store/actions/user";

class LoginPage extends React.Component {
  submit = data =>
    this.props.login(data).then(() => this.props.history.push("/"));

  render() {
    return (
      <div>
        <LoginForm submit={this.submit} />
      </div>
    );
  }
}

export default connect(
  null,
  { login }
)(LoginPage);
