import React from "react";
import PastActivityCard from "./PastActivityCard";
import EmptyPage from "../../components/partials/EmptyPage";

class ActivityList extends React.Component {
  state = {};

  render() {
    const meetingsList = (
      <div>
        <div>
          {this.props.activities.map(activity => (
            <PastActivityCard
              activity={activity}
              key={activity._id}
              deleteActivity={this.props.deleteActivity}
            />
          ))}
        </div>
      </div>
    );
    return (
      <div>
        {this.props.activities.length === 0 ? (
          <EmptyPage message="activities" />
        ) : (
          meetingsList
        )}
      </div>
    );
  }
}

export default ActivityList;
