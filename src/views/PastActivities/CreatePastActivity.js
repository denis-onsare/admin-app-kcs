import React from "react";
import {
  Container,
  Row,
  Col,
  Form,
  FormGroup,
  Input,
  Label,
  Card,
  CardHeader,
  CardBody,
  Button,
  FormFeedback
} from "reactstrap";

import { connect } from "react-redux";
import {
  postActivity,
  updateActivity,
  fetchActivityById
} from "../../store/actions/pastActivity";
import LoadingState from "../../components/states/LoadingState";

class CreatePastActivity extends React.Component {
  state = {
    data: {
      _id: this.props.activity ? this.props.activity._id : null,
      image: this.props.activity ? this.props.activity.image : [],
      description: this.props.activity ? this.props.activity.description : "",
      title: this.props.activity ? this.props.activity.title : "",
      venue: this.props.activity ? this.props.activity.content : "",
      date: this.props.activity ? this.props.activity.date : ""
    },
    errors: {},
    loading: false,
    progress: "awaiting uploads..."
  };
  componentDidMount = () => {
    const { match } = this.props;
    if (match.params._id) {
      this.props.fetchActivityById(match.params._id);
    }
  };

  componentWillReceiveProps = nextProps => {
    this.setState({
      data: {
        _id: nextProps.activity._id,
        title: nextProps.activity.title,
        venue: nextProps.activity.venue,
        description: nextProps.activity.description,
        image: nextProps.activity.image,
        date: nextProps.activity.date
      }
    });
  };

  handleFileUpload = e => {
    let image_as_base64 = URL.createObjectURL(e.target.files[0]);
    this.setState({ image: e.target.files[0], image_as_base64 });
  };

  onSubmit = e => {
    e.preventDefault();
    const errors = this.validate(this.state.data);
    const data = this.state.data;
    this.setState({ errors });
    let bodyFormData = new FormData();
    if (data._id) bodyFormData.set("_id", data._id);
    bodyFormData.set("description", data.description);
    bodyFormData.set("venue", data.venue);
    bodyFormData.set("title", data.title);
    bodyFormData.set("date", data.date);
    bodyFormData.append("image", this.state.image);
    if (Object.keys(errors).length === 0) {
      this.setState({ loading: true });
      if (data._id) {
        this.props
          .updateActivity(bodyFormData)
          .then(() => this.props.history.push("/past-activities"))
          .catch(err =>
            this.setState({ errors: err.response.data, loading: false })
          );
      } else {
        this.props
          .postActivity(bodyFormData)
          .then(() => this.props.history.push("/past-activities"))
          .catch(err =>
            this.setState({ errors: err.response.data, loading: false })
          );
      }
    }
  };

  onChange = e => {
    this.setState({
      data: { ...this.state.data, [e.target.name]: e.target.value }
    });
  };

  validate = data => {
    const errors = {};
    if (!data.title) errors.title = "title should not be empty";
    if (!data.date) errors.date = "date should not be empty";
    if (!data.venue) errors.venue = "venue should not be empty";
    if (!data.description) errors.description = "description is required";

    return errors;
  };

  render() {
    const { data, errors, loading } = this.state;

    return (
      <div>
        <Container>
          <Row>
            <Col>
              <Card>
                <CardHeader>Create Activity</CardHeader>
                <CardBody>
                  {loading && <LoadingState message="loading..." />}

                  <Form onSubmit={this.onSubmit}>
                    {errors.error && (
                      <div className="alert alert-danger">{errors.error}</div>
                    )}
                    <FormGroup row>
                      <div
                        style={{
                          border: "2px dotted #ccc",
                          height: "250px",
                          width: "500px",
                          overflow: "hidden",
                          display: "flex",
                          justifyContent: "center",
                          alignItems: "center"
                        }}
                      >
                        {this.state.image ? (
                          <img
                            width="100%"
                            src={this.state.image_as_base64}
                            alt=""
                          />
                        ) : (
                          <Label htmlFor="meeting-poster" sm={3}>
                            <span className="btn btn-success">
                              <i className="fa fa-upload"></i> Upload Poster
                            </span>
                          </Label>
                        )}
                      </div>
                      <Input
                        accept="image/*"
                        id="meeting-poster"
                        type="file"
                        name="image"
                        onChange={this.handleFileUpload}
                        // invalid={!!errors.image}
                        style={{ display: "none" }}
                      />
                      {/* <FormFeedback>{errors.image}</FormFeedback> */}
                    </FormGroup>
                    <FormGroup>
                      <Label>Title</Label>
                      <Input
                        type="text"
                        placeholder="activity title"
                        name="title"
                        onChange={this.onChange}
                        value={data.title}
                        invalid={!!errors.title}
                      />
                      <FormFeedback>{errors.title}</FormFeedback>
                    </FormGroup>
                    <FormGroup>
                      <Label>Date</Label>
                      <Input
                        type="date"
                        name="date"
                        value={data.date}
                        onChange={this.onChange}
                        invalid={!!errors.date}
                      />
                      <FormFeedback>{errors.date}</FormFeedback>
                    </FormGroup>
                    <FormGroup>
                      <Label>Venue</Label>
                      <Input
                        type="text"
                        placeholder="Enter activity venue"
                        name="venue"
                        value={data.venue}
                        onChange={this.onChange}
                        invalid={!!errors.venue}
                      />
                      <FormFeedback>{errors.venue}</FormFeedback>
                    </FormGroup>

                    <FormGroup>
                      <Label>Description</Label>
                      <Input
                        type="textarea"
                        style={{ height: 200 }}
                        placeholder="Describe the activity"
                        name="description"
                        value={data.description}
                        onChange={this.onChange}
                        invalid={!!errors.description}
                      />
                      <FormFeedback>{errors.description}</FormFeedback>
                    </FormGroup>
                    <Button>Submit</Button>
                  </Form>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

function mapStateToProps(state, props) {
  const { match } = props;
  if (match.params._id) {
    return {
      activity: state.activity.find(item => item._id === match.params._id)
    };
  }

  return { activity: null };
}

export default connect(mapStateToProps, {
  postActivity,
  updateActivity,
  fetchActivityById
})(CreatePastActivity);
