import React from "react";
import { connect } from "react-redux";
import { Container, Row, Card, CardHeader, CardBody, Col } from "reactstrap";

import {
  fetchActivity,
  deleteActivity
} from "../../store/actions/pastActivity";
import ActivityList from "./ActivitiesList";

class PastActivities extends React.Component {
  componentDidMount() {
    this.props.fetchActivity();
  }

  render() {
    return (
      <Container>
        <Row>
          <Col>
          <Card>
            <CardHeader>All Past Activities</CardHeader>
            <CardBody>
              <ActivityList
                activities={this.props.activities}
                deleteActivity={this.props.deleteActivity}
              />
            </CardBody>
          </Card>
          </Col>
        </Row>
      </Container>
    );
  }
}

function mapStateToProps(state) {
  return {
    activities: state.activity
  };
}

export default connect(
  mapStateToProps,
  { fetchActivity, deleteActivity }
)(PastActivities);
