import React from "react";
import {
  Container,
  Row,
  Col,
  Card,
  CardImg,
  CardBody,
  CardTitle,
  CardHeader,
  CardText,
  CardSubtitle,
  Button
} from "reactstrap";
import { Link } from "react-router-dom";

import { connect } from "react-redux";
import { fetchActivityById } from "../../store/actions/pastActivity";
import Moment from "react-moment";

class Activity extends React.Component {
  state = {
    activity: {
      _id: this.props.activity ? this.props.activity._id : null,
      image: this.props.activity ? this.props.activity.images[0] : "",
      description: this.props.activity ? this.props.activity.description : "",
      title: this.props.activity ? this.props.activity.title : "",
      venue: this.props.activity ? this.props.activity.content : "",
      date: this.props.activity ? this.props.activity.date : ""
    }
  };
  componentDidMount = () => {
    const { match } = this.props;
    if (match.params._id) {
      this.props.fetchActivityById(match.params._id);
    }
  };

  componentWillReceiveProps = nextProps => {
    this.setState({
      activity: {
        _id: nextProps.activity._id,
        title: nextProps.activity.title,
        venue: nextProps.activity.venue,
        description: nextProps.activity.description,
        image: nextProps.activity.images,
        date: nextProps.activity.date
      }
    });
  };

  render() {
    const { activity } = this.state;

    return (
      <Container>
        <Row className="mt40 mb40">
          <Col sm={{ size: "8", offset: "2" }}>
            <Card>
              <CardImg
                top
                width="100%"
                height="350px"
                src={activity.image}
                alt={activity.title}
              />
              <CardBody>
                <CardTitle>{activity.title}</CardTitle>
                <CardSubtitle>
                  Date <Moment format="YYYY/MM/DD">{activity.date}</Moment>
                </CardSubtitle>
                <CardText>{activity.description}</CardText>
                <div className="float-right">
                  <Link to={`/past-activities/edit/${activity._id}`}>
                    <Button color="success">Edit</Button>
                  </Link>
                </div>
              </CardBody>
            </Card>
          </Col>
        </Row>
        <Row className="mt40 mb40">
          <Col sm={{ size: "8", offset: "2" }}>
            <Card>
              <CardHeader>Comments</CardHeader>
              <CardBody>no comment</CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
    );
  }
}
function mapStateToProps(state, props) {
  const { match } = props;
  if (match.params._id) {
    return {
      activity: state.activity.find(item => item._id === match.params._id)
    };
  }

  return { activity: null };
}

export default connect(
  mapStateToProps,
  { fetchActivityById }
)(Activity);
