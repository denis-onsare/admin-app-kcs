import React from "react";
import { Row, Col, Button } from "reactstrap";
import Moment from "react-moment";
import { Link } from "react-router-dom";

const PastActivityCard = ({ activity, deleteActivity }) => (
  <Row className="mb20">
    <Col sm="5">
      <img
        src={activity.images[0]}
        width="100%"
        alt={activity.title}
      />
    </Col>
    <Col sm="7">
      <h6>Title: {activity.title}</h6>
      <h6>Description</h6>
      {activity.description}
      <h6>
        Date: <Moment format="YYYY/MM/DD">{activity.date}</Moment>{" "}
      </h6>
      <h6>Venue: {activity.venue}</h6>
      <div className="float-right">
        <Link to={`/past-activities/${activity._id}`}>
          <Button color="success" className="mr10">
            View Details
          </Button>
        </Link>

        <Button color="danger" onClick={() => deleteActivity(activity._id)}>
          Delete
        </Button>
      </div>
    </Col>
  </Row>
);

export default PastActivityCard;
