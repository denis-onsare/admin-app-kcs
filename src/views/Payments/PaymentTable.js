import React from "react";
import { Row, Col, Card, CardBody, Table, CardHeader } from "reactstrap";
import Moment from "react-moment";
import EmptyPage from "../../components/partials/EmptyPage";
import avatar from "../../assets/img/avatars/avatar.png";

const PaymentTable = ({ payments }) => {
  const paymentTable = (
    <Row className="mt20 mb20">
      <Col>
        <Card>
          <CardHeader>Payments</CardHeader>
          <CardBody>
            <br />
            <Table
              hover
              responsive
              className="table-outline mb-0 d-none d-sm-table"
            >
              <thead className="thead-light">
                <tr>
                  <th className="text-center">
                    <i className="icon-people" />
                  </th>
                  <th>TransID</th>
                  <th>TransactionType</th>
                  <th>TransTime</th>
                  <th>TransAmount</th>
                  <th>BusinessShortCode</th>
                  <th>BillRefNumber</th>
                  <th>InvoiceNumber</th>
                  <th>OrgAccountBalance</th>
                  <th>ThirdPartyTransID</th>
                  <th>MSISDN</th>
                  <th>Name</th>
                </tr>
              </thead>
              <tbody>
                {payments.map(trans => (
                  <tr key={trans.TransID}>
                    <td className="text-center">
                      <div className="avatar">
                        <img
                          src={avatar}
                          className="img-avatar"
                          alt={trans.TransID}
                          width="100%"
                        />
                      </div>
                    </td>
                    <td>
                      <div>{trans.TransID}</div>
                    </td>
                    <td>
                      <div>{trans.TransactionType}</div>
                    </td>
                    <td>
                      <div>
                        <Moment parse="YYYY-MM-DD HH:mm">
                          {trans.TransTime}
                        </Moment>
                      </div>
                    </td>
                    <td>
                      <div>{trans.TransAmount}</div>
                    </td>
                    <td>
                      <div>{trans.BusinessShortCode}</div>
                    </td>
                    <td>
                      <div>{trans.BillRefNumber}</div>
                    </td>
                    <td>
                      <div>{trans.InvoiceNumber}</div>
                    </td>
                    <td>
                      <div>{trans.OrgAccountBalance}</div>
                    </td>
                    <td>
                      <div>{trans.ThirdPartyTransID}</div>
                    </td>
                    <td>
                      <div>{trans.MSISDN}</div>
                    </td>
                    <td>
                      <div>
                        {trans.FirstName} {trans.MiddleName} {trans.LastName}
                      </div>
                    </td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </CardBody>
        </Card>
      </Col>
    </Row>
  );

  return (
    <div>
      {payments.length === 0 ? <EmptyPage message="payments" /> : paymentTable}
    </div>
  );
};

export default PaymentTable;
