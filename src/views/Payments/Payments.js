import React from "react";
import { Container, Col, Row } from "reactstrap";
import PaymentTable from "./PaymentTable";
import { connect } from "react-redux";
import { fetchPayments } from "../../store/actions/payments";

class Payments extends React.Component {
  state = {};

  componentDidMount() {
    this.props.fetchPayments();
  }

  render() {
    return (
      <Container>
        <Row className="pt40 pb40">
          <Col sm="12">
            <PaymentTable payments={this.props.payments} />
          </Col>
        </Row>
      </Container>
    );
  }
}

function mapStateToProps(state) {
  return {
    payments: state.payments
  };
}

export default connect(
  mapStateToProps,
  { fetchPayments }
)(Payments);
