import React from "react";
import {
  Container,
  Row,
  Col,
  Form,
  FormGroup,
  Input,
  Label,
  Card,
  CardHeader,
  CardBody,
  FormFeedback,
  Button
} from "reactstrap";
import LoadingState from "../../components/states/LoadingState";
// import ReactQuill from "react-quill";

import { connect } from "react-redux";
import {
  createPost,
  fetchPostById,
  updatePost
} from "../../store/actions/post";

class CreatePost extends React.Component {
  state = {
    data: {
      _id: this.props.post ? this.props.post._id : null,
      // image: this.props.post ? this.props.post.image : 0,
      author: this.props.author ? this.props.post.author : "",
      title: this.props.post ? this.props.post.title : "",
      excerpt: this.props.post ? this.props.post.excerpt : "",
      content: this.props.post ? this.props.post.content : "",
      createdAt: this.props.post ? this.props.post.createdAt : ""
    },

    loading: false,
    errors: {},
    chars_left: 140
  };

  componentDidMount = () => {
    const { match } = this.props;
    if (match.params._id) {
      this.props.fetchPostById(match.params._id);
    }
  };

  componentWillReceiveProps = nextProps => {
    this.setState({
      data: {
        _id: nextProps.post._id,
        title: nextProps.post.title,
        author: nextProps.post.author,
        content: nextProps.post.content,
        excerpt: nextProps.post.excerpt,
        image: nextProps.post.image,
        createdAt: nextProps.post.createdAt
      },
      errors: {},
      loading: false
    });
  };

  onSubmit = e => {
    e.preventDefault();
    const errors = this.validate(this.state.data);
    const data = this.state.data;
    this.setState({ errors });
    if (Object.keys(errors).length === 0) {
      this.setState({ loading: true });
      if (data._id) {
        this.props
          .updatePost(data)
          .then(() => this.props.history.push(`/post/${data._id}`))
          .catch(err =>
            this.setState({ errors: err.response.data.errors, loading: false })
          );
      } else {
        this.props
          .createPost(data)
          .then(() => this.props.history.push("/posts"))
          .catch(err =>
            this.setState({ errors: err.response.data, loading: false })
          );
      }
    }
  };

  onChange = e => {
    const charCount = e.target.value.length;
    const charLeft = 140 - charCount;
    this.setState({ chars_left: charLeft });

    this.setState({
      data: { ...this.state.data, [e.target.name]: e.target.value }
    });
  };

  validate = data => {
    const errors = {};

    if (!data.title) errors.title = "title is required";
    if (!data.author) errors.author = "author is required";
    if (!data.excerpt) errors.excerpt = "excerpt is required";
    if (!data.content) errors.content = "content is required";
    return errors;
  };

  render() {
    const { data, errors, loading } = this.state;
    return (
      <div>
        <Container>
          <Row>
            <Col>
              <Card>
                <CardHeader>New Post</CardHeader>
                <CardBody>
                  {loading && <LoadingState message="loading..." />}
                  <Form onSubmit={this.onSubmit}>
                    {errors.error && (
                      <div className="alert alert-danger">{errors.error}</div>
                    )}
                    <FormGroup>
                      <Label>Title</Label>
                      <Input
                        type="text"
                        placeholder="Post title"
                        name="title"
                        onChange={this.onChange}
                        value={data.title}
                        invalid={!!errors.title}
                      />
                      <FormFeedback>{errors.title}</FormFeedback>
                    </FormGroup>
                    <FormGroup>
                      <Label>Author</Label>
                      <Input
                        type="text"
                        placeholder="Post Author"
                        name="author"
                        onChange={this.onChange}
                        value={data.author}
                        invalid={!!errors.author}
                      />
                      <FormFeedback>{errors.author}</FormFeedback>
                    </FormGroup>
                    {/* <FormGroup>
                      <Label>Choose Post Image</Label>
                      <Input type="file" />
                      <FormFeedback>{errors.image}</FormFeedback>
                    </FormGroup> */}
                    <FormGroup>
                      <Label>Excerpt</Label>
                      <Input
                        type="textarea"
                        style={{ height: 100 }}
                        placeholder="A brief summary of the post"
                        name="excerpt"
                        maxLength={140}
                        onChange={this.onChange}
                        value={data.excerpt}
                        invalid={!!errors.excerpt}
                      />
                      <p> characters remaining:{this.state.chars_left}</p>
                      <FormFeedback>{errors.excerpt}</FormFeedback>
                    </FormGroup>
                    <FormGroup>
                      <Label>Content</Label>

                      <Input
                        type="textarea"
                        style={{ height: 200 }}
                        placeholder="post content"
                        name="content"
                        onChange={this.onChange}
                        value={data.content}
                        invalid={!!errors.content}
                      />
                      <FormFeedback>{errors.content}</FormFeedback>
                    </FormGroup>
                    <Button>Submit</Button>
                  </Form>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

// CreatePost.modules = {
//   toolbar: [
//     [{ header: [1, 2, false] }],
//     ["bold", "italic", "underline", "strike", "blockquote"],
//     [
//       { list: "ordered" },
//       { list: "bullet" },
//       { indent: "-1" },
//       { indent: "+1" }
//     ],
//     ["link", "image"],
//     ["clean"]
//   ],
//   clipboard: {
//     matchVisual: false
//   }
// };

// CreatePost.formats = [
//   "header",
//   "font",
//   "size",
//   "bold",
//   "italic",
//   "underline",
//   "strike",
//   "blockquote",
//   "list",
//   "bullet",
//   "indent",
//   "link",
//   "image",
//   "color"
// ];

function mapStateToProps(state, props) {
  const { match } = props;
  if (match.params._id) {
    return {
      post: state.post.find(item => item._id === match.params._id)
    };
  }

  return { post: null };
}

export default connect(
  mapStateToProps,
  { createPost, fetchPostById, updatePost }
)(CreatePost);
