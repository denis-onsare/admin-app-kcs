import React from "react";
import {
  Container,
  Row,
  Col,
  Card,
  CardImg,
  CardBody,
  CardTitle,
  CardHeader,
  CardText,
  CardSubtitle,
  Button
} from "reactstrap";
import { Link } from "react-router-dom";

import { connect } from "react-redux";
import { fetchPostById } from "../../store/actions/post";
import Moment from "react-moment";

class PostPage extends React.Component {
  state = {
    post: {
      _id: this.props.post ? this.props.post._id : null,
      image: this.props.post ? this.props.post.image : "",
      author: this.props.post ? this.props.post.author : "",
      title: this.props.post ? this.props.post.title : "",
      content: this.props.post ? this.props.post.content : "",
      createdAt: this.props.post ? this.props.post.createdAt : ""
    }
  };
  componentDidMount = () => {
    const { match } = this.props;
    if (match.params._id) {
      this.props.fetchPostById(match.params._id);
    }
  };

  componentWillReceiveProps = nextProps => {
    this.setState({
      post: {
        _id: nextProps.post._id,
        title: nextProps.post.title,
        author: nextProps.post.author,
        content: nextProps.post.content,
        image: nextProps.post.image,
        createdAt: nextProps.post.createdAt
      }
    });
  };

  render() {
    const { post } = this.state;

    return (
      <Container>
        <Row className="mt40 mb40">
          <Col sm={{ size: "8", offset: "2" }}>
            <Card>
              <CardImg
                top
                width="100%"
                height="350px"
                src={post.image}
                alt={post.title}
              />
              <CardBody>
                <CardTitle>{post.title}</CardTitle>
                <CardSubtitle>
                  Posted <Moment fromNow>{post.createdAt}</Moment> by{" "}
                  {post.author}
                </CardSubtitle>
                <CardText>{post.content}</CardText>
                <div className="float-right">
                  <Link to={`/posts/edit/${post._id}`}>
                    <Button color="success">Edit</Button>
                  </Link>
                </div>
              </CardBody>
            </Card>
          </Col>
        </Row>
        <Row className="mt40 mb40">
          <Col sm={{ size: "8", offset: "2" }}>
            <Card>
              <CardHeader>Comments</CardHeader>
              <CardBody>no comment</CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
    );
  }
}
function mapStateToProps(state, props) {
  const { match } = props;
  if (match.params._id) {
    return {
      post: state.post.find(item => item._id === match.params._id)
    };
  }

  return { post: null };
}

export default connect(
  mapStateToProps,
  { fetchPostById }
)(PostPage);
