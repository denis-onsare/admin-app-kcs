import React from "react";
import { Col, CardBody, Button, Row } from "reactstrap";
import { Link } from "react-router-dom";
import Moment from "react-moment";

const PostCard = ({ post, deletePost }) => (
  <div key={post._id}>
    <CardBody>
      <Row className="mb20" key={post._id}>
        <Col sm="6">
          <img src={post.image} width="100%" alt={post.title} />
        </Col>
        <Col sm="6">
          <h6>Title: {post.title}</h6>
          <h6>
            Posted By: {post.author} on{" "}
            <Moment format="DD/MM/YYYY">{post.createdAt}</Moment>
          </h6>
          <p>{post.excerpt}</p>
          <div className="float-right">
            <Link to={`/posts/${post._id}`}>
              <Button color="success" className="mr10">
                View Details
              </Button>
            </Link>
            &nbsp;
            <Button color="danger" onClick={() => deletePost(post._id)}>
              Delete
            </Button>
          </div>
        </Col>
      </Row>
    </CardBody>
  </div>
);

export default PostCard;
