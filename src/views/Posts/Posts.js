import React from "react";
import { connect } from "react-redux";
import { Container, Row, CardHeader, Card, Col } from "reactstrap";

import { fetchPosts, deletePost } from "../../store/actions/post";
import PostsList from "./PostsList";

class Posts extends React.Component {
  componentDidMount() {
    this.props.fetchPosts();
  }

  render() {
    return (
      <Container>
        <Row>
          <Col>
          <Card>
            <CardHeader>All Posts</CardHeader>
            <PostsList
              posts={this.props.posts}
              deletePost={this.props.deletePost}
            />
          </Card>
          </Col>
        </Row>
      </Container>
    );
  }
}

function mapStateToProps(state) {
  return {
    posts: state.post
  };
}

export default connect(
  mapStateToProps,
  { fetchPosts, deletePost }
)(Posts);
