import React from "react";
import EmptyPage from "../../components/partials/EmptyPage";
import PostCard from "./PostCard";

const PostsList = ({ posts, deletePost }) => {
  const postsList = (
    <div>
      {posts.map(post => (
        <PostCard post={post} key={post._id} deletePost={deletePost} />
      ))}
    </div>
  );

  return (
    <div>{posts.length === 0 ? <EmptyPage message="posts" /> : postsList}</div>
  );
};

export default PostsList;
