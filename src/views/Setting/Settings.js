import React from "react";
import { Container, Col, Row, Card, CardBody, CardHeader } from "reactstrap";

const Settings = () => (
  <Container>
    <Row className="pt40 pb40">
      <Col sm="6">
        <Card>
          <CardHeader> Settings</CardHeader>
          <CardBody>no settings available</CardBody>
        </Card>
      </Col>
    </Row>
  </Container>
);

export default Settings;
