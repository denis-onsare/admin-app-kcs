import React from "react";
import {
  Container,
  Row,
  Col,
  Form,
  FormGroup,
  Input,
  Label,
  Card,
  CardHeader,
  CardBody,
  Button,
  FormFeedback
} from "reactstrap";

import { connect } from "react-redux";
import {
  postTopicalIssue,
  fetchIssueById,
  updateIssue
} from "../../store/actions/topicalIssue";
import LoadingState from "../../components/states/LoadingState";

class CreateTopicalIssue extends React.Component {
  state = {
    data: {
      _id: this.props.issue ? this.props.issue._id : null,
      image: this.props.issue ? this.props.issue.image : [],
      description: this.props.issue ? this.props.issue.description : "",
      title: this.props.issue ? this.props.issue.title : ""
    },
    errors: {},
    loading: false,
    progress: "Awaiting uploads..."
  };
  componentDidMount = () => {
    const { match } = this.props;
    if (match.params._id) {
      this.props.fetchIssueById(match.params._id);
    }
  };

  componentWillReceiveProps = nextProps => {
    this.setState({
      issue: {
        _id: nextProps.issue._id,
        title: nextProps.issue.title,

        description: nextProps.issue.description,
        image: nextProps.issue.image
      }
    });
  };

  handleFileUpload = e => {
    let image_as_base64 = URL.createObjectURL(e.target.files[0]);
    this.setState({ image: e.target.files[0], image_as_base64 });
  };

  onSubmit = e => {
    e.preventDefault();
    const errors = this.validate(this.state.data);
    const data = this.state.data;
    this.setState({ errors });
    let bodyFormData = new FormData();
    if (data._id) bodyFormData.set("_id", data._id);
    bodyFormData.set("description", data.description);
    bodyFormData.set("title", data.title);
    bodyFormData.append("image", this.state.image);
    if (Object.keys(errors).length === 0) {
      this.setState({ loading: true });
      if (data._id) {
        this.props
          .updateIssue(bodyFormData)
          .then(() => this.props.history.push("/topical-issues"))
          .catch(err =>
            this.setState({ errors: err.response.data, loading: false })
          );
      } else {
        this.props
          .postTopicalIssue(bodyFormData)
          .then(() => this.props.history.push("/topical-issues"))
          .catch(err =>
            this.setState({ errors: err.response.data, loading: false })
          );
      }
    }
  };

  onChange = e => {
    this.setState({
      data: { ...this.state.data, [e.target.name]: e.target.value }
    });
  };

  validate = data => {
    const errors = {};
    if (!data.title) errors.title = "title should not be empty";

    if (!data.description) errors.description = "description is required";

    return errors;
  };

  render() {
    const { data, errors, loading } = this.state;

    return (
      <div>
        <Container>
          <Row>
            <Col>
              <Card>
                <CardHeader>Create Topical Issue</CardHeader>
                <CardBody>
                  {loading && <LoadingState message="loading..." />}

                  <Form onSubmit={this.onSubmit}>
                    {errors.error && (
                      <div className="alert alert-danger">{errors.error}</div>
                    )}
                    <FormGroup row>
                      <div
                        style={{
                          border: "2px dotted #ccc",
                          height: "250px",
                          width: "500px",
                          overflow: "hidden",
                          display: "flex",
                          justifyContent: "center",
                          alignItems: "center"
                        }}
                      >
                        {this.state.image ? (
                          <img
                            width="100%"
                            src={this.state.image_as_base64}
                            alt=""
                          />
                        ) : (
                          <Label htmlFor="meeting-poster" sm={3}>
                            <span className="btn btn-success">
                              <i className="fa fa-upload"></i> Upload Poster
                            </span>
                          </Label>
                        )}
                      </div>
                      <Input
                        accept="image/*"
                        id="meeting-poster"
                        type="file"
                        name="image"
                        onChange={this.handleFileUpload}
                        // invalid={!!errors.image}
                        style={{ display: "none" }}
                      />
                      {/* <FormFeedback>{errors.image}</FormFeedback> */}
                    </FormGroup>
                    <FormGroup>
                      <Label>Title</Label>
                      <Input
                        type="text"
                        placeholder="Issue title"
                        name="title"
                        onChange={this.onChange}
                        value={data.title}
                        invalid={!!errors.title}
                      />
                      <FormFeedback>{errors.title}</FormFeedback>
                    </FormGroup>

                    <FormGroup>
                      <Label>Description</Label>
                      <Input
                        type="textarea"
                        style={{ height: 200 }}
                        placeholder="Describe the activity"
                        name="description"
                        value={data.description}
                        onChange={this.onChange}
                        invalid={!!errors.description}
                      />
                      <FormFeedback>{errors.description}</FormFeedback>
                    </FormGroup>
                    <Button>Submit</Button>
                  </Form>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}
function mapStateToProps(state, props) {
  const { match } = props;
  if (match.params._id) {
    return {
      issue: state.topicalIssue.find(item => item._id === match.params._id)
    };
  }

  return { issue: null };
}

export default connect(mapStateToProps, {
  postTopicalIssue,
  fetchIssueById,
  updateIssue
})(CreateTopicalIssue);
