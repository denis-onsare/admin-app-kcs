import React from "react";
import TopicalIssueCard from "./TopicalIssueCard";
import EmptyPage from "../../components/partials/EmptyPage";

class IssuesList extends React.Component {
  state = {};

  render() {
    const issuesList = (
      <div>
        <div>
          {this.props.issues.map(issue => (
            <TopicalIssueCard
              issue={issue}
              key={issue._id}
              deleteIssue={this.props.deleteIssue}
            />
          ))}
        </div>
      </div>
    );
    return (
      <div>
        {this.props.issues.length === 0 ? (
          <EmptyPage message="issues" />
        ) : (
          issuesList
        )}
      </div>
    );
  }
}

export default IssuesList;
