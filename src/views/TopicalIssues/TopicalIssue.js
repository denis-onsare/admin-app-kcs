import React from "react";
import {
  Container,
  Row,
  Col,
  Card,
  CardImg,
  CardBody,
  CardTitle,
  CardHeader,
  CardText,
  CardSubtitle,
  Button
} from "reactstrap";
import { Link } from "react-router-dom";

import { connect } from "react-redux";
import { fetchIssueById } from "../../store/actions/topicalIssue";
import Moment from "react-moment";

class TopicalIssue extends React.Component {
  state = {
    issue: {
      _id: this.props.issue ? this.props.issue._id : null,
      image: this.props.issue ? this.props.issue.images[0] : "",
      description: this.props.issue ? this.props.issue.description : "",
      title: this.props.issue ? this.props.issue.title : ""
    }
  };
  componentDidMount = () => {
    const { match } = this.props;
    if (match.params._id) {
      this.props.fetchIssueById(match.params._id);
    }
  };

  componentWillReceiveProps = nextProps => {
    this.setState({
      issue: {
        _id: nextProps.issue._id,
        title: nextProps.issue.title,

        description: nextProps.issue.description,
        image: nextProps.issue.images
      }
    });
  };

  render() {
    const { issue } = this.state;

    return (
      <Container>
        <Row className="mt40 mb40">
          <Col sm={{ size: "8", offset: "2" }}>
            <Card>
              <CardImg
                top
                width="100%"
                height="350px"
                src={issue.image}
                alt={issue.title}
              />
              <CardBody>
                <CardTitle>{issue.title}</CardTitle>
                <CardSubtitle>
                  Date <Moment format="YYYY/MM/DD">{issue.date}</Moment>
                </CardSubtitle>
                <CardText>{issue.description}</CardText>
                <div className="float-right">
                  <Link to={`/topical-issues/edit/${issue._id}`}>
                    <Button color="success">Edit</Button>
                  </Link>
                </div>
              </CardBody>
            </Card>
          </Col>
        </Row>
        <Row className="mt40 mb40">
          <Col sm={{ size: "8", offset: "2" }}>
            <Card>
              <CardHeader>Comments</CardHeader>
              <CardBody>no comment</CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
    );
  }
}
function mapStateToProps(state, props) {
  const { match } = props;
  if (match.params._id) {
    return {
      issue: state.topicalIssue.find(item => item._id === match.params._id)
    };
  }

  return { issue: null };
}

export default connect(
  mapStateToProps,
  { fetchIssueById }
)(TopicalIssue);
