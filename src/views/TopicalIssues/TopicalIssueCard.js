import React from "react";
import { Row, Col, Button } from "reactstrap";
import { Link } from "react-router-dom";

const TopicalIssueCard = ({ issue, deleteIssue }) => (
  <Row className="mb20">
    <Col sm="5">
      <img src={issue.images[0]} width="100%" alt={issue.title} />
    </Col>
    <Col sm="7">
      <h6>Title: {issue.title}</h6>
      <h6>Description</h6>
      <div>{issue.description}</div>
      <br />
      <div className="float-right">
        <Link to={`/topical-issues/${issue._id}`}>
          <Button color="success" className="mr10">
            View Details
          </Button>
        </Link>

        <Button color="danger" onClick={() => deleteIssue(issue._id)}>
          Delete
        </Button>
      </div>
    </Col>
  </Row>
);

export default TopicalIssueCard;
