import React from "react";
import { connect } from "react-redux";
import { Container, Row, Card, CardHeader, CardBody , Col} from "reactstrap";

import { fetchIssues, deleteIssue } from "../../store/actions/topicalIssue";
import IssuesList from "./IssuesList";

class TopicalIssues extends React.Component {
  componentDidMount() {
    this.props.fetchIssues();
  }

  render() {
    return (
      <Container>
        <Row>
          <Col>
          <Card>
            <CardHeader>All Topical Issues</CardHeader>
            <CardBody>
              <IssuesList
                issues={this.props.issues}
                deleteIssue={this.props.deleteIssue}
              />
            </CardBody>
          </Card>
          </Col>
        </Row>
      </Container>
    );
  }
}

function mapStateToProps(state) {
  return {
    issues: state.topicalIssue
  };
}

export default connect(
  mapStateToProps,
  { fetchIssues, deleteIssue }
)(TopicalIssues);
