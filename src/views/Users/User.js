import React, { Component } from "react";
import {
  Card,
  CardBody,
  CardHeader,
  Col,
  Row,
  Table,
  Form,
  FormGroup,
  Label,
  Input,
  Button,
  FormFeedback
} from "reactstrap";
import { connect } from "react-redux";

import { fetchUsers, updateUser, deleteUser } from "../../store/actions/users";

class User extends Component {
  state = {
    data: {
      _id: this.props.match.params.id,
      status: "",
      paid: ""
    },
    errors: {}
  };
  onSubmit = e => {
    e.preventDefault();
    const data = this.state.data;
    const update = {
      _id: data._id,
      status: data.status,
      paid: data.paid === "Paid" ? true : false
    };
    const errors = this.validate(data);
    this.setState({ errors });
    console.log(this.state.errors);
    if (Object.keys(errors).length === 0) {
      this.props
        .updateUser(update)
        .then(() => this.props.history.push("/users"))
        .catch(err => this.setState({ errors: err.response.data }));
    }
  };

  validate = data => {
    const errors = {};

    if (data.status === "Choose An Option" || !data.status)
      errors.status = "Please choose a valid option";
    if (data.paid === "Choose An Option" || !data.paid)
      errors.paid = "Please choose a valid option";

    return errors;
  };

  onChange = e => {
    this.setState({
      data: { ...this.state.data, [e.target.name]: e.target.value }
    });
  };
  componentDidMount() {
    this.props.fetchUsers();
  }
  render() {
    const { data, errors } = this.state;
    const user = this.props.usersData.filter(
      user => user._id === this.props.match.params.id
    );

    // const user = this.props.usersData.find(
    //   user => user._id === this.props.match.params.id
    // );
    // const userDetails = user
    //   ? Object.entries(user)
    //   : [
    //       [
    //         "id",
    //         <span>
    //           <i className="text-muted icon-ban" /> Not found
    //         </span>
    //       ]
    //     ];

    return (
      <div className="animated fadeIn">
        <Row>
          <Col lg={6}>
            <Card>
              <CardHeader>
                <strong>
                  <i className="icon-info pr-1" />
                  User id: {this.props.match.params.id}
                </strong>
              </CardHeader>
              <CardBody>
                <Table responsive striped hover>
                  <tbody>
                    {user.map(user => {
                      return (
                        <React.Fragment key={user._id}>
                          <tr>
                            <td>
                              <strong>First Name: {user.firstName}</strong>
                            </td>
                            <td>
                              <strong>Middle Name: {user.middleName}</strong>
                            </td>
                            <td>
                              <strong>Last Name: {user.lastName}</strong>
                            </td>
                          </tr>
                          <tr>
                            <td>
                              <strong>Title: {user.title}</strong>
                            </td>
                            <td>
                              <strong>Phone: {user.phoneNumber}</strong>
                            </td>
                            <td>
                              <strong>Email: {user.email}</strong>
                            </td>
                          </tr>
                          <tr>
                            <td>
                              <strong>Membership: {user.membership}</strong>
                            </td>
                            <td>
                              <strong>Chapter: {user.chapter}</strong>
                            </td>
                            <td>
                              <strong>Status: {user.status}</strong>
                            </td>
                          </tr>
                          <tr>
                            <td>
                              <strong>Role: {user.role}</strong>
                            </td>
                            <td>
                              <strong>
                                Fee: {user.paid === true ? "YES" : "NO"}
                              </strong>
                            </td>
                            <td>
                              <strong>Handle: {user.handle}</strong>
                            </td>
                          </tr>
                        </React.Fragment>
                      );
                    })}
                  </tbody>
                </Table>
              </CardBody>
            </Card>
          </Col>
          <Col lg={6}>
            <Card>
              <CardHeader>
                <strong>
                  <i className="fa fa-wrench " /> Admin Actions
                </strong>
              </CardHeader>
              <CardBody>
                <Form onSubmit={this.onSubmit}>
                  {errors.error && (
                    <div className="alert alert-danger">{errors.error}</div>
                  )}
                  <FormGroup>
                    <Label>User ID</Label>
                    <Input
                      type="text"
                      name="_id"
                      value={data._id}
                      onChange={this.onChange}
                      disabled
                    />
                  </FormGroup>
                  <FormGroup row>
                    <Label sm={6}>Change User Status</Label>
                    <Col sm={6}>
                      <Input
                        type="select"
                        name="status"
                        onChange={this.onChange}
                        value={data.status}
                        invalid={!!errors.status}
                      >
                        <option>Choose An Option</option>
                        <option>Pending</option>
                        <option>Active</option>
                        <option>Inactive</option>
                        <option>Banned</option>
                      </Input>

                      <FormFeedback>{errors.status}</FormFeedback>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Label sm={6}>Change User Payment StatuS</Label>
                    <Col sm={6}>
                      <Input
                        type="select"
                        name="paid"
                        onChange={this.onChange}
                        value={data.paid}
                        invalid={!!errors.paid}
                      >
                        <option>Choose An Option</option>
                        <option>Paid</option>
                        <option>Not Paid</option>
                      </Input>
                      <FormFeedback>{errors.paid}</FormFeedback>
                    </Col>
                  </FormGroup>
                  <Button color="success">Save Changes</Button>
                  &nbsp;
                  <Button
                    color="danger"
                    onClick={() =>
                      this.props
                        .deleteUser(data._id)
                        .then(() => this.props.history.push("/users"))
                        .catch(err =>
                          this.setState({ errors: err.response.data })
                        )
                    }
                  >
                    Delete User
                  </Button>
                </Form>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    usersData: state.users
  };
}

export default connect(mapStateToProps, { fetchUsers, updateUser, deleteUser })(
  User
);
