import React, { Component } from "react";
import { Badge, Card, CardBody, CardHeader, Col, Row, Table } from "reactstrap";

import { connect } from "react-redux";
import { fetchUsers } from "../../store/actions/users";

function UserRow(props) {
  const user = props.user;
  const userLink = `/users/${user._id}`;

  const getBadge = status => {
    return status === "Active"
      ? "success"
      : status === "Inactive"
      ? "secondary"
      : status === "Pending"
      ? "warning"
      : status === "Banned"
      ? "danger"
      : "primary";
  };

  return (
    <tr key={user._id.toString()}>
      <td>
        <a href={userLink}>
          {user.title} {user.firstName} {user.lastName}
        </a>
      </td>
      <th scope="row">
        <a href={userLink}>{user.email}</a>
      </th>
      <th scope="row">
        <a href={userLink}>0{user.phoneNumber}</a>
      </th>
      <td>{new Date(user.createdAt).toDateString()}</td>
      <td>{user.role}</td>
      <td>
        <Badge href={userLink} color={getBadge(user.status)}>
          {user.status}
        </Badge>
      </td>
    </tr>
  );
}

class Users extends Component {
  componentDidMount() {
    this.props.fetchUsers();
  }

  render() {
    return (
      <div className="animated fadeIn">
        <Row>
          <Col>
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify" /> Users{" "}
              </CardHeader>
              <CardBody>
                <Table responsive hover>
                  <thead>
                    <tr>
                      <th scope="col">Name</th>
                      <th scope="col">Email</th>
                      <th scope="col">Phone</th>
                      <th scope="col">Joined</th>
                      <th scope="col">Role</th>
                      <th scope="col">Status</th>
                    </tr>
                  </thead>
                  <tbody>
                    {this.props.usersData.map(user => (
                      <UserRow key={user._id} user={user} />
                    ))}
                  </tbody>
                </Table>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    usersData: state.users
  };
}

export default connect(mapStateToProps, { fetchUsers })(Users);
